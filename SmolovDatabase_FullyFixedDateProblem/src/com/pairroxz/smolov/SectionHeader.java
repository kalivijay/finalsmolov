package com.pairroxz.smolov;

public class SectionHeader implements Item{

	public String title;
	
	public SectionHeader(String title) {
		this.title=title;
	}
	
	@Override
	public boolean isSection() {
		return true;
	}

	@Override
	public boolean isURL() {
		return false;
	}

}
