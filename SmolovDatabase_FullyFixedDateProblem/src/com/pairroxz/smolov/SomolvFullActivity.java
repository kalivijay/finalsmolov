package com.pairroxz.smolov;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class SomolvFullActivity extends Activity {

	TextView txtTtitle5, textonerep, textincrement;
	TextView textheader, textday, textsets, textlift, textreps, textweight,
			textdone;
	ListView listview1;
	ImageView imgIcon;
	ImageButton backButton;
	EditText edittext1, edittext2;
	String edittext1_Text;
	String edittext2_Text;
	CheckBox checkbox1;
	static String click_position_full;

	ArrayList<Item> items = new ArrayList<Item>();

	ArrayList wt = new ArrayList();
	ArrayList wh = new ArrayList();
	SmolovFullAdapter smolovFullAdapter;

	static String wt0 = "0.0";
	static String wt1 = "0.0";
	static String wt2 = "0.0";
	static String wt3 = "0.0";
	static String wt4 = "0.0";
	static String wt5 = "0.0";
	static String wt6 = "0.0";
	static String wt7 = "0.0";
	static String wt8 = "0.0";
	static String wt9 = "0.0";
	static String wt10 = "0.0";
	static String wt11 = "0.0";
	static String wt12 = "0.0";
	static String wt13 = "0.0";
	static String wt14 = "0.0";
	static String wt15 = "0.0";
	static String wt16 = "0.0";
	static String wt17 = "0.0";
	static String wt18 = "0.0";
	static String wt19 = "0.0";
	static String wt20 = "0.0";

	static String wtextra = "na";
	SunFull Sun_data[];

	private SharedPreferences prefs;
	private SharedPreferences.Editor prefs_edit;

	// static boolean onceTime = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);

		Typeface font1 = Typeface.createFromAsset(getAssets(),
				"fonts/Seravek.otf");

		setContentView(R.layout.activity_somolv_full);

		prefs = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		prefs_edit = prefs.edit();

		// imgIcon = (ImageView) findViewById(R.id.imgIcon);
		edittext1 = (EditText) findViewById(R.id.edittext1);
		edittext2 = (EditText) findViewById(R.id.edittext2);
		edittext1.setText(prefs.getString("oneRepFull", ""));
		edittext2.setText(prefs.getString("incrementFull", "5"));

		// if (Settings.reset_all.equals("25")) {
		// prefs_edit.clear();
		// prefs_edit.commit();
		// Settings.reset_all = "0";
		// }
		//
		textheader = (TextView) findViewById(R.id.textheader);
		textday = (TextView) findViewById(R.id.textdayfull);
		textsets = (TextView) findViewById(R.id.textsetsfull);
		textreps = (TextView) findViewById(R.id.textrepsfull);
		textweight = (TextView) findViewById(R.id.textweightfull);
		textlift = (TextView) findViewById(R.id.textliftfull);
		textdone = (TextView) findViewById(R.id.textdonefull);
		textonerep = (TextView) findViewById(R.id.textonerepfull);
		textincrement = (TextView) findViewById(R.id.textincrementfull);

		textheader.setTypeface(font1);
		textday.setTypeface(font1);
		textsets.setTypeface(font1);
		textreps.setTypeface(font1);
		textlift.setTypeface(font1);
		textdone.setTypeface(font1);
		textonerep.setTypeface(font1);
		textincrement.setTypeface(font1);
		textweight.setTypeface(font1);

		backButton = (ImageButton) findViewById(R.id.backButton);

		listview1 = (ListView) findViewById(R.id.listView1);

		items.add(new SectionHeader("Week 1 - Phase In"));
		items.add(new SmolovFull("1", "Day 1", "1", "3", "8", "0.0", false));
		items.add(new SmolovFull("2", "", "2", "1", "5", "0.0", false));
		items.add(new SmolovFull("3", "", "3", "2", "2", "0.0", false));
		items.add(new SmolovFull("4", "", "4", "1", "1", "0.0", false));
		items.add(new SmolovFull("5", "Day 2", "1", "3", "8", "0.0", false));
		items.add(new SmolovFull("6", "", "2", "1", "5", "0.0", false));
		items.add(new SmolovFull("7", "", "3", "2", "2", "0.0", false));
		items.add(new SmolovFull("8", "", "4", "1", "1", "0.0", false));
		items.add(new SmolovFull("9", "Day 3", "1", "4", "5", "0.0", false));
		items.add(new SmolovFull("10", "", "2", "1", "3", "0.0", false));
		items.add(new SmolovFull("11", "", "3", "2", "2", "0.0", false));
		items.add(new SmolovFull("12", "", "4", "1", "1", "0.0", false));
		items.add(new SectionHeader("Week 2 - Phase In"));
		items.add(new SmolovFull("13", "Day 1", "1", "1", "5", "0.0", false));
		items.add(new SmolovFull("14", "Day 2", "1", "1", "5", "0.0", false));
		items.add(new SmolovFull("15", "Day 3", "1", "1", "5", "0.0", false));
		items.add(new SectionHeader("Week 3 - Base Cycle"));
		items.add(new SmolovFull("16", "Day 1", "1", "4", "9", "0.0", false));
		items.add(new SmolovFull("17", "Day 2", "1", "5", "7", "0.0", false));
		items.add(new SmolovFull("18", "Day 3", "1", "7", "5", "0.0", false));
		items.add(new SmolovFull("19", "Day 4", "1", "10", "3", "0.0", false));
		items.add(new SectionHeader("Week 4 - Base Cycle"));
		items.add(new SmolovFull("20", "Day 1", "1", "4", "9", "0.0", false));
		items.add(new SmolovFull("21", "Day 2", "1", "5", "7", "0.0", false));
		items.add(new SmolovFull("22", "Day 3", "1", "7", "5", "0.0", false));
		items.add(new SmolovFull("23", "Day 4", "1", "10", "3", "0.0", false));
		items.add(new SectionHeader("Week 5 - Base Cycle"));
		items.add(new SmolovFull("24", "Day 1", "1", "4", "9", "0.0", false));
		items.add(new SmolovFull("25", "Day 2", "1", "5", "7", "0.0", false));
		items.add(new SmolovFull("26", "Day 3", "1", "7", "5", "0.0", false));
		items.add(new SmolovFull("27", "Day 4", "1", "10", "3", "0.0", false));
		items.add(new SectionHeader("Week 6 - Base Cycle"));
		items.add(new SmolovFull("28", "Day 1", "1", "0", "0", "0.0", false));
		items.add(new SmolovFull("29", "Day 2", "1", "0", "0", "0.0", false));
		items.add(new SmolovFull("30", "Day 3", "1", "1", "1", "0.0", false));
		items.add(new SmolovFull("31", "Day 4", "1", "1", "1", "0.0", false));
		items.add(new SectionHeader("Week 7 - Switching Cycle"));
		items.add(new URLText("See smolovjr.com for details"));
		items.add(new SectionHeader("Week 8 - Switching Cycle"));
		items.add(new URLText("See smolovjr.com for details"));
		items.add(new SectionHeader("Week 9 - Intense Cycle"));
		items.add(new SmolovFull("32", "Day 1", "1", "1", "3", "0.0", false));
		items.add(new SmolovFull("33", "", "2", "1", "4", "0.0", false));
		items.add(new SmolovFull("34", "", "3", "3", "4", "0.0", false));
		items.add(new SmolovFull("35", "", "4", "1", "5", "0.0", false));
		items.add(new SmolovFull("36", "Day 2", "1", "1", "3", "0.0", false));
		items.add(new SmolovFull("37", "", "2", "1", "3", "0.0", false));
		items.add(new SmolovFull("38", "", "3", "1", "4", "0.0", false));
		items.add(new SmolovFull("39", "", "4", "1", "3", "0.0", false));
		items.add(new SmolovFull("40", "", "5", "2", "5", "0.0", false));
		items.add(new SmolovFull("41", "Day 3", "1", "1", "4", "0.0", false));
		items.add(new SmolovFull("42", "", "2", "1", "4", "0.0", false));
		items.add(new SmolovFull("43", "", "3", "5", "4", "0.0", false));
		items.add(new SectionHeader("Week 10 - Intense Cycle"));
		items.add(new SmolovFull("44", "Day 1", "1", "1", "4", "0.0", false));
		items.add(new SmolovFull("45", "", "2", "1", "4", "0.0", false));
		items.add(new SmolovFull("46", "", "3", "1", "4", "0.0", false));
		items.add(new SmolovFull("47", "", "4", "1", "3", "0.0", false));
		items.add(new SmolovFull("48", "", "5", "2", "4", "0.0", false));
		items.add(new SmolovFull("49", "Day 2", "1", "1", "3", "0.0", false));
		items.add(new SmolovFull("50", "", "2", "1", "3", "0.0", false));
		items.add(new SmolovFull("51", "", "3", "1", "3", "0.0", false));
		items.add(new SmolovFull("52", "", "4", "3", "3", "0.0", false));
		items.add(new SmolovFull("53", "", "5", "1", "3", "0.0", false));
		items.add(new SmolovFull("54", "Day 3", "1", "1", "3", "0.0", false));
		items.add(new SmolovFull("55", "", "2", "1", "3", "0.0", false));
		items.add(new SmolovFull("56", "", "3", "1", "4", "0.0", false));
		items.add(new SmolovFull("57", "", "4", "4", "5", "0.0", false));
		items.add(new SectionHeader("Week 11 - Intense Cycle"));
		items.add(new SmolovFull("58", "Day 1", "1", "1", "3", "0.0", false));
		items.add(new SmolovFull("59", "", "2", "1", "3", "0.0", false));
		items.add(new SmolovFull("60", "", "3", "1", "3", "0.0", false));
		items.add(new SmolovFull("61", "", "4", "5", "5", "0.0", false));
		items.add(new SmolovFull("62", "Day 2", "1", "1", "3", "0.0", false));
		items.add(new SmolovFull("63", "", "2", "1", "3", "0.0", false));
		items.add(new SmolovFull("64", "", "3", "1", "3", "0.0", false));
		items.add(new SmolovFull("65", "", "4", "2", "3", "0.0", false));
		items.add(new SmolovFull("66", "Day 3", "1", "1", "3", "0.0", false));
		items.add(new SmolovFull("67", "", "2", "1", "3", "0.0", false));
		items.add(new SmolovFull("68", "", "3", "1", "3", "0.0", false));
		items.add(new SmolovFull("69", "", "4", "4", "5", "0.0", false));
		items.add(new SectionHeader("Week 12 - Intense Cycle"));
		items.add(new SmolovFull("70", "Day 1", "1", "1", "3", "0.0", false));
		items.add(new SmolovFull("71", "", "2", "1", "4", "0.0", false));
		items.add(new SmolovFull("72", "", "3", "5", "5", "0.0", false));
		items.add(new SmolovFull("73", "Day 2", "1", "1", "3", "0.0", false));
		items.add(new SmolovFull("74", "", "2", "1", "3", "0.0", false));
		items.add(new SmolovFull("75", "", "3", "4", "3", "0.0", false));
		items.add(new SmolovFull("76", "Day 3", "1", "1", "3", "0.0", false));
		items.add(new SmolovFull("77", "", "2", "1", "4", "0.0", false));
		items.add(new SmolovFull("78", "", "3", "3", "4", "0.0", false));
		items.add(new SectionHeader("Week 13 - Tapper Week"));
		items.add(new SmolovFull("79", "Day 1", "1", "1", "3", "0.0", false));
		items.add(new SmolovFull("80", "", "2", "1", "3", "0.0", false));
		items.add(new SmolovFull("81", "", "3", "2", "5", "0.0", false));
		items.add(new SmolovFull("82", "", "4", "3", "4", "0.0", false));
		items.add(new SmolovFull("83", "Day 2", "1", "1", "4", "0.0", false));
		items.add(new SmolovFull("84", "", "2", "4", "4", "0.0", false));
		items.add(new SmolovFull("85", "Day 3", "1", "1", "1", "0.0", false));

		smolovFullAdapter = new SmolovFullAdapter(SomolvFullActivity.this,
				items);
		listview1.setAdapter(smolovFullAdapter);

		listview1.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub

				if (position != 0 && position != 13 && position != 17
						&& position != 22 && position != 27 && position != 32
						&& position != 37 && position != 39 && position != 41
						&& position != 54 && position != 69 && position != 82
						&& position != 92) {
					if (position == 38 || position == 40) {
						Uri uri = Uri.parse("http://www.smolovjr.com/");
						Intent intent = new Intent(Intent.ACTION_VIEW, uri);
						startActivity(intent);
					} else {
						checkbox1 = (CheckBox) view
								.findViewById(R.id.checkbox1);

						click_position_full = "" + position;
						Intent ep_MyIntent;

						if (checkbox1.isChecked()) {

							ep_MyIntent = new Intent(SomolvFullActivity.this,
									DayComplete_Full.class);
							startActivity(ep_MyIntent);
						}

						else if (!checkbox1.isChecked()) {
							ep_MyIntent = new Intent(SomolvFullActivity.this,
									GetAfterItActivity_Full.class);
							startActivity(ep_MyIntent);
						}
					}
				}
			}

		});

		edittext1.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
				txtTtitle5 = (TextView) findViewById(R.id.txtTitle5);
				edittext1_Text = edittext1.getText().toString();
				prefs_edit.putString("oneRepFull", edittext1_Text);
				edittext2_Text = edittext2.getText().toString();
				prefs_edit.putString("incrementFull", edittext2_Text);
				prefs_edit.commit();
				calculations();

			}
		});
		edittext2.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(edittext2.getWindowToken(), 0);

			}
		});

		edittext2.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				txtTtitle5 = (TextView) findViewById(R.id.txtTitle5);
				edittext1_Text = edittext1.getText().toString();
				prefs_edit.putString("oneRepFull", edittext1_Text);
				edittext2_Text = edittext2.getText().toString();
				prefs_edit.putString("incrementFull", edittext2_Text);
				prefs_edit.commit();

				calculations();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		backButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		calculations();
	}

	public void calculations() {
		edittext1_Text = edittext1.getText().toString();
		edittext2_Text = edittext2.getText().toString();
		if (edittext1_Text.length() != 0 && edittext2_Text.length() != 0) {
			int value1 = Integer.parseInt(edittext1_Text);
			int value2 = Integer.parseInt(edittext2_Text);

			float firstStep = 0;
			int secondStep = 0;
			float final_value1 = 0;

			// *****************Calculations Started***Week 1, 2 and 3*******

			// ********Calcualte Wt0**60%************

			firstStep = (float) ((value1 * 0.6) / 2.5);
			// firstStep = Math.round(firstStep);

			// float test = 1.4f;
			int abc = (int) firstStep;
			if (firstStep - 0.5 == abc || firstStep - 0.51f == abc
					|| firstStep - 0.52f == abc || firstStep - 0.53f == abc
					|| firstStep - 0.54f == abc || firstStep - 0.55f == abc
					|| firstStep - 0.56f == abc || firstStep - 0.57f == abc
					|| firstStep - 0.58f == abc || firstStep - 0.59f == abc) {
				secondStep = abc;
			} else {
				secondStep = Math.round(firstStep);
			}

			final_value1 = (float) (secondStep * 2.5);

			wt0 = final_value1 + "";

			// ********Calcualte Wt1**65%************

			firstStep = (float) ((value1 * 0.65) / 2.5);
			// firstStep = Math.round(firstStep);

			// float test = 1.4f;
			abc = (int) firstStep;
			if (firstStep - 0.5 == abc || firstStep - 0.51f == abc
					|| firstStep - 0.52f == abc || firstStep - 0.53f == abc
					|| firstStep - 0.54f == abc || firstStep - 0.55f == abc
					|| firstStep - 0.56f == abc || firstStep - 0.57f == abc
					|| firstStep - 0.58f == abc || firstStep - 0.59f == abc) {
				secondStep = abc;
			} else {
				secondStep = Math.round(firstStep);
			}

			final_value1 = (float) (secondStep * 2.5);

			wt1 = final_value1 + "";

			// ********Calcualte Wt2** 70% ************

			firstStep = (float) ((value1 * 0.7) / 2.5);

			abc = (int) firstStep;
			if (firstStep - 0.5 == abc || firstStep - 0.51f == abc
					|| firstStep - 0.52f == abc || firstStep - 0.53f == abc
					|| firstStep - 0.54f == abc || firstStep - 0.55f == abc
					|| firstStep - 0.56f == abc || firstStep - 0.57f == abc
					|| firstStep - 0.58f == abc || firstStep - 0.59f == abc) {
				secondStep = abc;
			} else {
				secondStep = Math.round(firstStep);
			}

			final_value1 = (float) (secondStep * 2.5);

			wt2 = final_value1 + "";

			// ********Calcualte Wt3** 75% ************

			firstStep = (float) ((value1 * 0.75) / 2.5);

			abc = (int) firstStep;
			if (firstStep - 0.5 == abc || firstStep - 0.51f == abc
					|| firstStep - 0.52f == abc || firstStep - 0.53f == abc
					|| firstStep - 0.54f == abc || firstStep - 0.55f == abc
					|| firstStep - 0.56f == abc || firstStep - 0.57f == abc
					|| firstStep - 0.58f == abc || firstStep - 0.59f == abc) {
				secondStep = abc;
			} else {
				secondStep = Math.round(firstStep);
			}

			final_value1 = (float) (secondStep * 2.5);

			wt3 = final_value1 + "";

			// ********Calcualte Wt4** 80% ************

			firstStep = (float) ((value1 * 0.80) / 2.5);

			abc = (int) firstStep;
			if (firstStep - 0.5 == abc || firstStep - 0.51f == abc
					|| firstStep - 0.52f == abc || firstStep - 0.53f == abc
					|| firstStep - 0.54f == abc || firstStep - 0.55f == abc
					|| firstStep - 0.56f == abc || firstStep - 0.57f == abc
					|| firstStep - 0.58f == abc || firstStep - 0.59f == abc) {
				secondStep = abc;
			} else {
				secondStep = Math.round(firstStep);
			}

			final_value1 = (float) (secondStep * 2.5);

			wt4 = final_value1 + "";

			// ********Calcualte Wt5** 90% ************

			firstStep = (float) ((value1 * 0.90) / 2.5);

			abc = (int) firstStep;

			if (firstStep - 0.5 == abc || firstStep - 0.51f == abc
					|| firstStep - 0.52f == abc || firstStep - 0.53f == abc
					|| firstStep - 0.54f == abc || firstStep - 0.55f == abc
					|| firstStep - 0.56f == abc || firstStep - 0.57f == abc
					|| firstStep - 0.58f == abc || firstStep - 0.59f == abc
					|| firstStep - 0.51f == abc || firstStep - 0.52f == abc
					|| firstStep - 0.53f == abc || firstStep - 0.54f == abc
					|| firstStep - 0.55f == abc || firstStep - 0.56f == abc
					|| firstStep - 0.57f == abc || firstStep - 0.58f == abc
					|| firstStep - 0.59f == abc) {
				secondStep = abc;
			} else {

				secondStep = Math.round(firstStep);

			}

			final_value1 = (float) (secondStep * 2.5);

			wt5 = final_value1 + "";

			// ********Calcualte Wt16** 95% ************
			firstStep = (float) ((value1 * 0.95) / 2.5);

			abc = (int) firstStep;
			if (firstStep - 0.5 == abc || firstStep - 0.51f == abc
					|| firstStep - 0.52f == abc || firstStep - 0.53f == abc
					|| firstStep - 0.54f == abc || firstStep - 0.55f == abc
					|| firstStep - 0.56f == abc || firstStep - 0.57f == abc
					|| firstStep - 0.58f == abc || firstStep - 0.59f == abc) {
				secondStep = abc;
			} else {
				secondStep = Math.round(firstStep);
			}

			final_value1 = (float) (secondStep * 2.5);

			wt16 = final_value1 + "";

			// ********Calcualte Wt6** 82.5% ************
			firstStep = (float) ((value1 * 0.825) / 2.5);

			abc = (int) firstStep;
			if (firstStep - 0.5 == abc || firstStep - 0.51f == abc
					|| firstStep - 0.52f == abc || firstStep - 0.53f == abc
					|| firstStep - 0.54f == abc || firstStep - 0.55f == abc
					|| firstStep - 0.56f == abc || firstStep - 0.57f == abc
					|| firstStep - 0.58f == abc || firstStep - 0.59f == abc) {
				secondStep = abc;
			} else {
				secondStep = Math.round(firstStep);
			}

			final_value1 = (float) (secondStep * 2.5);

			wt6 = final_value1 + "";

			// ********Calcualte Wt7** 85% ************
			firstStep = (float) ((value1 * 0.85) / 2.5);

			abc = (int) firstStep;
			if (firstStep - 0.5 == abc || firstStep - 0.51f == abc
					|| firstStep - 0.52f == abc || firstStep - 0.53f == abc
					|| firstStep - 0.54f == abc || firstStep - 0.55f == abc
					|| firstStep - 0.56f == abc || firstStep - 0.57f == abc
					|| firstStep - 0.58f == abc || firstStep - 0.59f == abc) {
				secondStep = abc;
			} else {
				secondStep = Math.round(firstStep);
			}

			final_value1 = (float) (secondStep * 2.5);

			wt7 = final_value1 + "";

			// *********************Week 4************************

			float wt2_in_int = (float) Double.parseDouble(wt2);

			final_value1 = wt2_in_int + value2;

			wt8 = final_value1 + "";

			float wt3_in_int = (float) Double.parseDouble(wt3);

			final_value1 = wt3_in_int + value2;

			wt9 = final_value1 + "";

			float wt4_in_int = (float) Double.parseDouble(wt4);

			final_value1 = wt4_in_int + value2;

			wt10 = final_value1 + "";

			float wt7_in_int = (float) Double.parseDouble(wt7);

			final_value1 = wt7_in_int + value2;

			wt11 = final_value1 + "";

			// *********************Week 5************************

			wt2_in_int = (float) Double.parseDouble(wt2);

			final_value1 = wt2_in_int + (value2 * 2);

			wt12 = final_value1 + "";

			wt3_in_int = (float) Double.parseDouble(wt3);

			final_value1 = wt3_in_int + (value2 * 2);

			wt13 = final_value1 + "";

			wt4_in_int = (float) Double.parseDouble(wt4);

			final_value1 = wt4_in_int + (value2 * 2);

			wt14 = final_value1 + "";

			wt7_in_int = (float) Double.parseDouble(wt7);

			final_value1 = wt7_in_int + (value2 * 2);

			wt15 = final_value1 + "";

			((SmolovFull) items.get(1)).weight = wt1;
			((SmolovFull) items.get(2)).weight = wt2;
			((SmolovFull) items.get(3)).weight = wt3;
			((SmolovFull) items.get(4)).weight = wt4;
			((SmolovFull) items.get(5)).weight = wt1;
			((SmolovFull) items.get(6)).weight = wt2;
			((SmolovFull) items.get(7)).weight = wt3;
			((SmolovFull) items.get(8)).weight = wt4;
			((SmolovFull) items.get(9)).weight = wt2;
			((SmolovFull) items.get(10)).weight = wt3;
			((SmolovFull) items.get(11)).weight = wt4;
			((SmolovFull) items.get(12)).weight = wt5;
			((SmolovFull) items.get(14)).weight = wt4;
			((SmolovFull) items.get(15)).weight = wt6;
			((SmolovFull) items.get(16)).weight = wt7;
			((SmolovFull) items.get(18)).weight = wt2;
			((SmolovFull) items.get(19)).weight = wt3;
			((SmolovFull) items.get(20)).weight = wt4;
			((SmolovFull) items.get(21)).weight = wt7;
			((SmolovFull) items.get(23)).weight = wt8;
			((SmolovFull) items.get(24)).weight = wt9;
			((SmolovFull) items.get(25)).weight = wt10;
			((SmolovFull) items.get(26)).weight = wt11;
			((SmolovFull) items.get(28)).weight = wt12;
			((SmolovFull) items.get(29)).weight = wt13;
			((SmolovFull) items.get(30)).weight = wt14;
			((SmolovFull) items.get(31)).weight = wt15;
			((SmolovFull) items.get(33)).weight = "Rest";
			((SmolovFull) items.get(34)).weight = "Rest";
			((SmolovFull) items.get(35)).weight = "Max";
			((SmolovFull) items.get(36)).weight = "Max";
			((SmolovFull) items.get(42)).weight = wt1;
			((SmolovFull) items.get(43)).weight = wt3;
			((SmolovFull) items.get(44)).weight = wt7;
			((SmolovFull) items.get(45)).weight = wt5;
			((SmolovFull) items.get(46)).weight = wt0;
			((SmolovFull) items.get(47)).weight = wt2;
			((SmolovFull) items.get(48)).weight = wt4;
			((SmolovFull) items.get(49)).weight = wt5;
			((SmolovFull) items.get(50)).weight = wt7;
			((SmolovFull) items.get(51)).weight = wt1;
			((SmolovFull) items.get(52)).weight = wt2;
			((SmolovFull) items.get(53)).weight = wt4;
			((SmolovFull) items.get(55)).weight = wt0;
			((SmolovFull) items.get(56)).weight = wt2;
			((SmolovFull) items.get(57)).weight = wt4;
			((SmolovFull) items.get(58)).weight = wt5;
			((SmolovFull) items.get(59)).weight = wt5;
			((SmolovFull) items.get(60)).weight = wt1;
			((SmolovFull) items.get(61)).weight = wt3;
			((SmolovFull) items.get(62)).weight = wt7;
			((SmolovFull) items.get(63)).weight = wt5;
			((SmolovFull) items.get(64)).weight = wt16;
			((SmolovFull) items.get(65)).weight = wt1;
			((SmolovFull) items.get(66)).weight = wt3;
			((SmolovFull) items.get(67)).weight = wt7;
			((SmolovFull) items.get(68)).weight = wt5;
			((SmolovFull) items.get(70)).weight = wt0;
			((SmolovFull) items.get(71)).weight = wt2;
			((SmolovFull) items.get(72)).weight = wt4;
			((SmolovFull) items.get(73)).weight = wt5;
			((SmolovFull) items.get(74)).weight = wt0;
			((SmolovFull) items.get(75)).weight = wt2;
			((SmolovFull) items.get(76)).weight = wt4;
			((SmolovFull) items.get(77)).weight = wt16;
			((SmolovFull) items.get(78)).weight = wt1;
			((SmolovFull) items.get(79)).weight = wt3;
			((SmolovFull) items.get(80)).weight = wt7;
			((SmolovFull) items.get(81)).weight = wt16;
			((SmolovFull) items.get(83)).weight = wt2;
			((SmolovFull) items.get(84)).weight = wt4;
			((SmolovFull) items.get(85)).weight = wt5;
			((SmolovFull) items.get(86)).weight = wt2;
			((SmolovFull) items.get(87)).weight = wt4;
			((SmolovFull) items.get(88)).weight = wt16;
			((SmolovFull) items.get(89)).weight = wt3;
			((SmolovFull) items.get(90)).weight = wt5;
			((SmolovFull) items.get(91)).weight = wt4;
			((SmolovFull) items.get(93)).weight = wt2;
			((SmolovFull) items.get(94)).weight = wt4;
			((SmolovFull) items.get(95)).weight = wt5;
			((SmolovFull) items.get(96)).weight = wt16;
			((SmolovFull) items.get(97)).weight = wt3;
			((SmolovFull) items.get(98)).weight = wt7;
			((SmolovFull) items.get(99)).weight = "Max";
			smolovFullAdapter.notifyDataSetChanged();

		}

	}

	@Override
	public void onBackPressed() {
		startActivity(new Intent(SomolvFullActivity.this, MainActivity.class));
		 finish();
	}

}
