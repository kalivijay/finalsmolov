package com.pairroxz.smolov;

public class SunFull {
	// public int icon;
	public String title1;
	public String title2;
	public String title3;
	public String title4;
	public String title5;
	public boolean selected;
	public String peh_id;
	public boolean header;

	public SunFull(String title1, String title2, String title3, String title4,
			String title5, boolean selected, String peh_id,boolean header) {
		super();
		// this.icon = icon;
		this.title1 = title1;
		this.title2 = title2;
		this.title3 = title3;
		this.title4 = title4;
		this.title5 = title5;
		this.selected = selected;
		this.peh_id = peh_id;
		this.header=header;
	}
}