package com.pairroxz.smolov;

public class URLText implements Item{

	public String url;
	
	public URLText(String url) {
		this.url=url;
	}
	
	@Override
	public boolean isSection() {
		return false;
	}

	@Override
	public boolean isURL() {
		return true;
	}

}
