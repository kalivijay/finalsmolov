package com.pairroxz.smolov;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

public class SmolovFullAdapter extends ArrayAdapter<Item> implements
		OnCheckedChangeListener {

	private List<Item> items;
	private LayoutInflater inflater;
	private SmolovFull smolovFull;
	private SectionHeader header;
	private URLText url;
	private SmolovHolder smolovHolder;
	private SectionHeaderHolder sectionHolder;
	private URLHolder urlHolder;
	private SharedPreferences prefs;
	private SharedPreferences.Editor prefs_edit;
	private Boolean[] arrChecks;
	private String check_list;
	Typeface font1;

	SimpleDateFormat take_date = new SimpleDateFormat("MM/dd/yyyy");
	String system_date = take_date.format(new Date());

	static Boolean[] store_date_f;

	public SmolovFullAdapter(Context context, List<Item> items) {
		super(context, 0, items);

		font1 = Typeface.createFromAsset(context.getAssets(),
				"fonts/Seravek.otf");

		this.items = items;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		prefs = PreferenceManager.getDefaultSharedPreferences(context
				.getApplicationContext());
		prefs_edit = prefs.edit();
		// if (Settings.reset_all.equals("25")) {
		// prefs_edit.clear();
		// prefs_edit.commit();
		// Settings.reset_all = "0";
		// }

		check_list = prefs.getString("check_list_2", null);
		arrChecks = new Boolean[items.size()];
		for (int i = 0; i < items.size(); i++) {
			arrChecks[i] = false;
		}

		if (check_list != null) {
			check_list = check_list.replace("[", "").replace("]", "")
					.replace(" ", "");
			String strTmpArr[] = check_list.split(",");
			for (int i = 0; i < items.size(); i++) {
				arrChecks[i] = Boolean.parseBoolean(strTmpArr[i]);
			}
		}
		store_date_f = arrChecks;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		final Item item = items.get(position);
		if (item.isSection()) {
			header = (SectionHeader) item;
			row = inflater.inflate(R.layout.listview_secton, null);
			sectionHolder = new SectionHeaderHolder(row);
			sectionHolder.txHeader.setText(header.title);
		} else if (item.isURL()) {
			url = (URLText) item;
			row = inflater.inflate(R.layout.textview_smolov, null);
			urlHolder = new URLHolder(row);
			urlHolder.txtURL.setText(url.url);
		} else {
			smolovFull = (SmolovFull) item;
			row = inflater.inflate(R.layout.listview_item_row_full, null);
			smolovHolder = new SmolovHolder(row);
			smolovHolder.txtTitle1.setText(smolovFull.day);
			smolovHolder.txtTitle2.setText(smolovFull.lift);
			smolovHolder.txtTitle3.setText(smolovFull.sets);
			smolovHolder.txtTitle4.setText(smolovFull.reps);
			smolovHolder.txtTitle5.setText(smolovFull.weight);
			smolovHolder.checkbox1.setTag(position);
			smolovHolder.checkbox1.setChecked(arrChecks[position]);
			smolovHolder.checkbox1.setOnCheckedChangeListener(this);
		}
		return row;
	}

	class SmolovHolder {
		TextView txtTitle1, txtTitle2, txtTitle3, txtTitle4, txtTitle5;
		CheckBox checkbox1;

		public SmolovHolder(View view) {
			txtTitle1 = (TextView) view.findViewById(R.id.txtTitle1);
			txtTitle2 = (TextView) view.findViewById(R.id.txtTitle2);
			txtTitle3 = (TextView) view.findViewById(R.id.txtTitle3);
			txtTitle4 = (TextView) view.findViewById(R.id.txtTitle4);
			txtTitle5 = (TextView) view.findViewById(R.id.txtTitle5);
			checkbox1 = (CheckBox) view.findViewById(R.id.checkbox1);

			txtTitle1.setTypeface(font1);
			txtTitle2.setTypeface(font1);
			txtTitle3.setTypeface(font1);
			txtTitle4.setTypeface(font1);
			txtTitle5.setTypeface(font1);
		}
	}

	class SectionHeaderHolder {
		TextView txHeader;

		public SectionHeaderHolder(View view) {
			txHeader = (TextView) view.findViewById(R.id.txtHeader);

			txHeader.setTypeface(font1);
		}
	}

	class URLHolder {
		TextView txtURL;

		public URLHolder(View view) {
			txtURL = (TextView) view.findViewById(R.id.textview_for_web);
			txtURL.setTypeface(font1);
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton checkbox, boolean checked) {
		arrChecks[Integer.parseInt(checkbox.getTag().toString())] = checked;
		String tempArrStr = Arrays.toString(arrChecks);

		store_date_f = arrChecks;

		// Started put "" in each checkbox postion
		if (!store_date_f[1]) {

			prefs_edit.putString("datef1", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef1", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef1", system_date);
				prefs_edit.commit();
			}

		}
		// ------------------------
		if (!store_date_f[2]) {

			prefs_edit.putString("datef2", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef2", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef2", system_date);
				prefs_edit.commit();
			}

		}
		// ------------------
		if (!store_date_f[3]) {

			prefs_edit.putString("datef3", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef3", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef3", system_date);
				prefs_edit.commit();
			}
		}
		// -----------------------

		if (!store_date_f[4]) {

			prefs_edit.putString("datef4", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef4", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef4", system_date);
				prefs_edit.commit();
			}
		}
		// --------------------------

		if (!store_date_f[5]) {

			prefs_edit.putString("datef5", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef5", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef5", system_date);
				prefs_edit.commit();
			}
		}
		// -------------------------

		if (!store_date_f[6]) {

			prefs_edit.putString("datef6", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef6", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef6", system_date);
				prefs_edit.commit();
			}
		}
		// ---------------------

		if (!store_date_f[7]) {

			prefs_edit.putString("datef7", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef7", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef7", system_date);
				prefs_edit.commit();
			}
		}
		// -----------------------------

		if (!store_date_f[8]) {

			prefs_edit.putString("datef8", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef8", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef8", system_date);
				prefs_edit.commit();
			}
		}
		// ------------------------------

		if (!store_date_f[9]) {

			prefs_edit.putString("datef9", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef9", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef9", system_date);
				prefs_edit.commit();
			}
		}
		// -----------------
		if (!store_date_f[10]) {

			prefs_edit.putString("datef10", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef10", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef10", system_date);
				prefs_edit.commit();
			}
		}
		// ---------------------

		if (!store_date_f[11]) {

			prefs_edit.putString("datef11", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef11", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef11", system_date);
				prefs_edit.commit();
			}

		}
		// ----------------------------

		if (!store_date_f[12]) {

			prefs_edit.putString("datef12", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef12", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef12", system_date);
				prefs_edit.commit();
			}

		}
		// -------------------------------

		if (!store_date_f[14]) {

			prefs_edit.putString("datef14", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef14", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef14", system_date);
				prefs_edit.commit();
			}
		}
		// -------------------------

		if (!store_date_f[15]) {

			prefs_edit.putString("datef15", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef15", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef15", system_date);
				prefs_edit.commit();
			}

		}
		// ---------------------------

		if (!store_date_f[16]) {

			prefs_edit.putString("datef16", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef16", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef16", system_date);
				prefs_edit.commit();
			}
		}
		// ---------------------------

		// -----------------1-----TO-----16----------
		if (!store_date_f[18]) {

			prefs_edit.putString("datef18", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef18", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef18", system_date);
				prefs_edit.commit();
			}
		}
		// ---------------------

		if (!store_date_f[19]) {

			prefs_edit.putString("datef19", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef19", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef19", system_date);
				prefs_edit.commit();
			}
		}
		// ---------------------------------

		if (!store_date_f[20]) {

			prefs_edit.putString("datef20", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef20", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef20", system_date);
				prefs_edit.commit();
			}
		}
		// -------------------------

		if (!store_date_f[21]) {

			prefs_edit.putString("datef21", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef21", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef21", system_date);
				prefs_edit.commit();
			}
		}
		// ----------------------------

		// ----- Till--------21----------
		if (!store_date_f[23]) {

			prefs_edit.putString("datef23", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef23", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef23", system_date);
				prefs_edit.commit();
			}

		}
		// ------------------------------

		if (!store_date_f[24]) {

			prefs_edit.putString("datef24", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef24", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef24", system_date);
				prefs_edit.commit();
			}
		}
		// --------------------------------

		if (!store_date_f[25]) {

			prefs_edit.putString("datef25", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef25", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef25", system_date);
				prefs_edit.commit();
			}
		}
		// ---------------------

		if (!store_date_f[26]) {

			prefs_edit.putString("datef26", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef26", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef26", system_date);
				prefs_edit.commit();
			}
		}
		// ----------------------

		// ------------Till-------26----------
		if (!store_date_f[28]) {

			prefs_edit.putString("datef28", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef28", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef28", system_date);
				prefs_edit.commit();
			}

		}
		// --------------------------------

		if (!store_date_f[29]) {

			prefs_edit.putString("datef29", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef29", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef29", system_date);
				prefs_edit.commit();
			}

		}

		if (!store_date_f[30]) {

			prefs_edit.putString("datef30", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef30", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef30", system_date);
				prefs_edit.commit();
			}

		}

		if (!store_date_f[31]) {

			prefs_edit.putString("datef31", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef31", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef31", system_date);
				prefs_edit.commit();
			}
		}

		// ------------Till-------31----------
		if (!store_date_f[33]) {

			prefs_edit.putString("datef33", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef33", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef33", system_date);
				prefs_edit.commit();
			}

		}

		if (!store_date_f[34]) {

			prefs_edit.putString("datef34", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef34", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef34", system_date);
				prefs_edit.commit();
			}

		}

		if (!store_date_f[35]) {

			prefs_edit.putString("datef35", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef35", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef35", system_date);
				prefs_edit.commit();
			}
		}

		if (!store_date_f[36]) {

			prefs_edit.putString("datef36", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef36", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef36", system_date);
				prefs_edit.commit();
			}

		}

		// ------------Till-------36----------
		if (!store_date_f[42]) {

			prefs_edit.putString("datef42", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef42", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef42", system_date);
				prefs_edit.commit();
			}

		}

		if (!store_date_f[43]) {

			prefs_edit.putString("datef43", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef43", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef43", system_date);
				prefs_edit.commit();
			}

		}

		if (!store_date_f[44]) {

			prefs_edit.putString("datef44", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef44", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef44", system_date);
				prefs_edit.commit();
			}

		}

		if (!store_date_f[45]) {

			prefs_edit.putString("datef45", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef45", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef45", system_date);
				prefs_edit.commit();
			}

		}

		if (!store_date_f[46]) {

			prefs_edit.putString("datef46", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef46", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef46", system_date);
				prefs_edit.commit();
			}

		}

		if (!store_date_f[47]) {

			prefs_edit.putString("datef47", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef47", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef47", system_date);
				prefs_edit.commit();
			}

		}

		if (!store_date_f[48]) {

			prefs_edit.putString("datef48", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef48", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef48", system_date);
				prefs_edit.commit();
			}
		}

		if (!store_date_f[49]) {

			prefs_edit.putString("datef49", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef49", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef49", system_date);
				prefs_edit.commit();
			}
		}

		if (!store_date_f[50]) {

			prefs_edit.putString("datef50", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef50", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef50", system_date);
				prefs_edit.commit();
			}
		}

		if (!store_date_f[51]) {

			prefs_edit.putString("datef51", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef51", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef51", system_date);
				prefs_edit.commit();
			}
		}

		if (!store_date_f[52]) {

			prefs_edit.putString("datef52", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef52", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef52", system_date);
				prefs_edit.commit();
			}
		}

		if (!store_date_f[53]) {

			prefs_edit.putString("datef53", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef53", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef53", system_date);
				prefs_edit.commit();
			}
		}

		// ------------Till-------53----------
		if (!store_date_f[55]) {

			prefs_edit.putString("datef55", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef55", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef55", system_date);
				prefs_edit.commit();
			}
		}

		if (!store_date_f[56]) {

			prefs_edit.putString("datef56", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef56", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef56", system_date);
				prefs_edit.commit();
			}

		}

		if (!store_date_f[57]) {

			prefs_edit.putString("datef57", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef57", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef57", system_date);
				prefs_edit.commit();
			}

		}

		if (!store_date_f[58]) {

			prefs_edit.putString("datef58", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef58", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef58", system_date);
				prefs_edit.commit();
			}

		}

		if (!store_date_f[59]) {

			prefs_edit.putString("datef59", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef59", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef59", system_date);
				prefs_edit.commit();
			}

		}

		if (!store_date_f[60]) {

			prefs_edit.putString("datef60", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef60", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef60", system_date);
				prefs_edit.commit();
			}

		}

		if (!store_date_f[61]) {

			prefs_edit.putString("datef61", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef61", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef61", system_date);
				prefs_edit.commit();
			}
		}

		if (!store_date_f[62]) {

			prefs_edit.putString("datef62", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef62", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef62", system_date);
				prefs_edit.commit();
			}

		}

		if (!store_date_f[63]) {

			prefs_edit.putString("datef63", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef63", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef63", system_date);
				prefs_edit.commit();
			}

		}

		if (!store_date_f[64]) {

			prefs_edit.putString("datef64", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef64", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef64", system_date);
				prefs_edit.commit();
			}
		}

		if (!store_date_f[65]) {

			prefs_edit.putString("datef65", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef65", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef65", system_date);
				prefs_edit.commit();

			}
		}

		if (!store_date_f[66]) {

			prefs_edit.putString("datef66", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef66", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef66", system_date);
				prefs_edit.commit();
			}

		}

		if (!store_date_f[67]) {

			prefs_edit.putString("datef67", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef67", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef67", system_date);
				prefs_edit.commit();
			}
		}

		if (!store_date_f[68]) {

			prefs_edit.putString("datef68", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef68", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef68", system_date);
				prefs_edit.commit();
			}

		}

		// ------------Till-------68----------
		if (!store_date_f[70]) {

			prefs_edit.putString("datef70", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef70", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef70", system_date);
				prefs_edit.commit();
			}

		}

		if (!store_date_f[71]) {

			prefs_edit.putString("datef71", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef71", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef71", system_date);
				prefs_edit.commit();
			}

		}

		if (!store_date_f[72]) {

			prefs_edit.putString("datef72", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef72", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef72", system_date);
				prefs_edit.commit();
			}

		}

		if (!store_date_f[73]) {

			prefs_edit.putString("datef73", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef73", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef73", system_date);
				prefs_edit.commit();
			}

		}

		if (!store_date_f[74]) {

			prefs_edit.putString("datef74", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef74", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef74", system_date);
				prefs_edit.commit();
			}

		}

		if (!store_date_f[75]) {

			prefs_edit.putString("datef75", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef75", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef75", system_date);
				prefs_edit.commit();
			}
		}

		if (!store_date_f[76]) {

			prefs_edit.putString("datef76", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef76", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef76", system_date);
				prefs_edit.commit();
			}

		}

		if (!store_date_f[77]) {

			prefs_edit.putString("datef77", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef77", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef77", system_date);
				prefs_edit.commit();
			}

		}

		if (!store_date_f[78]) {

			prefs_edit.putString("datef78", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef78", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef78", system_date);
				prefs_edit.commit();
			}

		}

		if (!store_date_f[79]) {

			prefs_edit.putString("datef79", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef79", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef79", system_date);
				prefs_edit.commit();
			}
		}

		if (!store_date_f[80]) {

			prefs_edit.putString("datef80", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef80", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef80", system_date);
				prefs_edit.commit();
			}

		}

		if (!store_date_f[81]) {

			prefs_edit.putString("datef81", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef81", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef81", system_date);
				prefs_edit.commit();
			}
		}

		// ------------Till-------81----------
		if (!store_date_f[83]) {

			prefs_edit.putString("datef83", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef83", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef83", system_date);
				prefs_edit.commit();
			}
		}

		if (!store_date_f[84]) {

			prefs_edit.putString("datef84", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef84", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef84", system_date);
				prefs_edit.commit();
			}
		}

		if (!store_date_f[85]) {

			prefs_edit.putString("datef85", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef85", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef85", system_date);
				prefs_edit.commit();
			}
		}

		if (!store_date_f[86]) {

			prefs_edit.putString("datef86", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef86", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef86", system_date);
				prefs_edit.commit();
			}
		}

		if (!store_date_f[87]) {

			prefs_edit.putString("datef87", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef87", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef87", system_date);
				prefs_edit.commit();
			}
		}

		if (!store_date_f[88]) {

			prefs_edit.putString("datef88", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef88", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef88", system_date);
				prefs_edit.commit();
			}
		}

		if (!store_date_f[89]) {

			prefs_edit.putString("datef89", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef89", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef89", system_date);
				prefs_edit.commit();
			}
		}

		if (!store_date_f[90]) {

			prefs_edit.putString("datef90", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef90", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef90", system_date);
				prefs_edit.commit();
			}
		}

		if (!store_date_f[91]) {

			prefs_edit.putString("datef91", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef91", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef91", system_date);
				prefs_edit.commit();
			}

		}

		// ------------Till-------91----------
		if (!store_date_f[93]) {

			prefs_edit.putString("datef93", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef93", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef93", system_date);
				prefs_edit.commit();
			}
		}

		if (!store_date_f[94]) {

			prefs_edit.putString("datef94", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef94", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef94", system_date);
				prefs_edit.commit();
			}
		}

		if (!store_date_f[95]) {

			prefs_edit.putString("datef95", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef95", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef95", system_date);
				prefs_edit.commit();
			}
		}

		if (!store_date_f[96]) {

			prefs_edit.putString("datef96", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef96", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef96", system_date);
				prefs_edit.commit();
			}
		}

		if (!store_date_f[97]) {

			prefs_edit.putString("datef97", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef97", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef97", system_date);
				prefs_edit.commit();
			}
		}

		if (!store_date_f[98]) {

			prefs_edit.putString("datef98", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef98", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef98", system_date);
				prefs_edit.commit();
			}
		}

		if (!store_date_f[99]) {

			prefs_edit.putString("datef99", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("datef99", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef99", system_date);
				prefs_edit.commit();
			}
		}

		// ------------Till-------99----------
		// Completed

		prefs_edit.putString("check_list_2", tempArrStr);
		prefs_edit.commit();

	}
}
