package com.pairroxz.smolov;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Settings extends Activity {

	ImageView backButton, resetApp, tellaFriend, more_about_jr,
			more_about_full;
	TextView textview1;

	String loginPrefs;

	public static String reset_all = "0", reset_all2 = "0";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getWindow().requestFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.activity_settings);
		Typeface font1 = Typeface.createFromAsset(getAssets(),
				"fonts/Seravek.otf");

		backButton = (ImageView) findViewById(R.id.backButton);
		resetApp = (ImageView) findViewById(R.id.reset);
		tellaFriend = (ImageView) findViewById(R.id.tell_a_friend);
		more_about_jr = (ImageView) findViewById(R.id.more_about_jr);
		more_about_full = (ImageView) findViewById(R.id.more_about_full);
		textview1 = (TextView) findViewById(R.id.textview1);

		textview1.setTypeface(font1);

		backButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
		more_about_jr.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Uri uri = Uri
						.parse("http://www.smolovjr.com/smolov-squat-program");
				Intent intent = new Intent(Intent.ACTION_VIEW, uri);
				startActivity(intent);
			}
		});
		more_about_full.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Uri uri = Uri
						.parse("http://www.smolovjr.com/smolov-squat-routine");
				Intent intent = new Intent(Intent.ACTION_VIEW, uri);
				startActivity(intent);

			}
		});

		tellaFriend.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(android.content.Intent.ACTION_SEND);
				intent.setType("text/plain");
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

				// Add data to the intent, the receiving app will decide what to
				// do with it.
				intent.putExtra(Intent.EXTRA_SUBJECT, "Some Subject Line");
				intent.putExtra(Intent.EXTRA_TEXT, "http://www.smolovjr.com/");

				startActivity(Intent.createChooser(intent, "Tell a Friend"));
			}
		});

		resetApp.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				// reset_all = "25";

				AlertDialog.Builder builder = new AlertDialog.Builder(
						Settings.this);
				builder.setTitle("Confirm");
				builder.setMessage("Are you sure want to delete all Data ?");
				builder.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub

								SharedPreferences prefs = PreferenceManager
										.getDefaultSharedPreferences(getApplicationContext());
								SharedPreferences.Editor prefs_edit = prefs
										.edit();
								prefs_edit.putString("check_list_1", null);
								prefs_edit.putString("check_list_2", null);
								prefs_edit.putString("oneRepFull", null);
								prefs_edit.putString("incrementFull", null);
								prefs_edit.putString("oneRepJr", null);
								prefs_edit.putString("incrementJr", null);

								prefs_edit.putString("date1", "");
								prefs_edit.putString("date2", "");
								prefs_edit.putString("date3", "");
								prefs_edit.putString("date4", "");
								prefs_edit.putString("date6", "");
								prefs_edit.putString("date7", "");
								prefs_edit.putString("date8", "");
								prefs_edit.putString("date9", "");
								prefs_edit.putString("date11", "");
								prefs_edit.putString("date12", "");
								prefs_edit.putString("date13", "");
								prefs_edit.putString("date14", "");

								prefs_edit.putString("datef1", "");
								prefs_edit.putString("datef2", "");
								prefs_edit.putString("datef3", "");
								prefs_edit.putString("datef4", "");
								prefs_edit.putString("datef6", "");
								prefs_edit.putString("datef7", "");
								prefs_edit.putString("datef8", "");
								prefs_edit.putString("datef9", "");
								prefs_edit.putString("datef11", "");
								prefs_edit.putString("datef12", "");
								prefs_edit.putString("datef14", "");
								prefs_edit.putString("datef15", "");
								prefs_edit.putString("datef16", "");
								prefs_edit.putString("datef18", "");
								prefs_edit.putString("datef19", "");
								prefs_edit.putString("datef20", "");
								prefs_edit.putString("datef21", "");
								prefs_edit.putString("datef23", "");
								prefs_edit.putString("datef24", "");
								prefs_edit.putString("datef25", "");
								prefs_edit.putString("datef26", "");
								prefs_edit.putString("datef28", "");
								prefs_edit.putString("datef29", "");
								prefs_edit.putString("datef30", "");
								prefs_edit.putString("datef31", "");
								prefs_edit.putString("datef33", "");
								prefs_edit.putString("datef34", "");
								prefs_edit.putString("datef35", "");
								prefs_edit.putString("datef36", "");

								prefs_edit.putString("datef42", "");
								prefs_edit.putString("datef43", "");
								prefs_edit.putString("datef44", "");
								prefs_edit.putString("datef45", "");
								prefs_edit.putString("datef46", "");
								prefs_edit.putString("datef47", "");
								prefs_edit.putString("datef48", "");
								prefs_edit.putString("datef49", "");
								prefs_edit.putString("datef50", "");
								prefs_edit.putString("datef51", "");
								prefs_edit.putString("datef52", "");
								prefs_edit.putString("datef53", "");
								prefs_edit.putString("datef55", "");
								prefs_edit.putString("datef56", "");
								prefs_edit.putString("datef57", "");
								prefs_edit.putString("datef58", "");
								prefs_edit.putString("datef59", "");
								prefs_edit.putString("datef60", "");
								prefs_edit.putString("datef61", "");
								prefs_edit.putString("datef62", "");
								prefs_edit.putString("datef63", "");
								prefs_edit.putString("datef64", "");
								prefs_edit.putString("datef65", "");
								prefs_edit.putString("datef66", "");
								prefs_edit.putString("datef67", "");
								prefs_edit.putString("datef68", "");
								prefs_edit.putString("datef70", "");
								prefs_edit.putString("datef71", "");
								prefs_edit.putString("datef72", "");
								prefs_edit.putString("datef73", "");
								prefs_edit.putString("datef74", "");
								prefs_edit.putString("datef75", "");
								prefs_edit.putString("datef76", "");
								prefs_edit.putString("datef77", "");
								prefs_edit.putString("datef78", "");
								prefs_edit.putString("datef79", "");
								prefs_edit.putString("datef80", "");
								prefs_edit.putString("datef81", "");
								prefs_edit.putString("datef83", "");
								prefs_edit.putString("datef84", "");
								prefs_edit.putString("datef85", "");
								prefs_edit.putString("datef86", "");
								prefs_edit.putString("datef87", "");
								prefs_edit.putString("datef88", "");
								prefs_edit.putString("datef89", "");
								prefs_edit.putString("datef90", "");
								prefs_edit.putString("datef91", "");
								prefs_edit.putString("datef93", "");
								prefs_edit.putString("datef94", "");
								prefs_edit.putString("datef95", "");
								prefs_edit.putString("datef96", "");
								prefs_edit.putString("datef97", "");
								prefs_edit.putString("datef98", "");
								prefs_edit.putString("datef99", "");

								prefs_edit.commit();

								// reset_all = "25";
								// reset_all2 = "25";
								Toast.makeText(getApplicationContext(),
										"Data Cleared", Toast.LENGTH_SHORT)
										.show();

							}
						});
				builder.setNegativeButton("No",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub

							}
						});

				AlertDialog alert = builder.create();
				alert.show();

			}
		});
	}

	@Override
	public void onBackPressed() {
		Intent myIntent = new Intent(Settings.this, MainActivity.class);
		startActivity(myIntent);
		finish();
	}
}
