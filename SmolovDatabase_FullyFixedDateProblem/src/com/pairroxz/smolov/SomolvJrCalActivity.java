package com.pairroxz.smolov;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class SomolvJrCalActivity extends Activity {

	TextView txtTtitle4, txtTtitle1, textonerep, textincrement;
	TextView textview1, textday, textsets, textreps, textweight, textdone;
	
	static int myview;

	ListView listview1;
	String weight;
	ImageView imgIcon;
	ImageButton backButton;
	CheckBox checkbox1;
	EditText edittext1, edittext2;
	int peh = 5;
	static String Wt1 = "0.0";
	static String Wt2 = "0.0";
	static String Wt3 = "0.0";
	static String Wt4 = "0.0";
	static String Wt5 = "0.0";
	static String Wt6 = "0.0";
	static String Wt7 = "0.0";
	static String Wt8 = "0.0";
	static String Wt9 = "0.0";
	static String Wt10 = "0.0";
	static String Wt11 = "0.0";
	static String Wt12 = "0.0";
	static String click_position;

	int FirstEdittextvalue1;
	String edittext1_Text;
	String edittext2_Text;

	// SharedPreferences Variables
	private SharedPreferences prefs;
	private SharedPreferences.Editor prefs_edit;
	Boolean checkbox_boolean, on_load_time_checkbox;
	String loginPrefs = "hi";
	String vph2;
	// SharedPreferences Variables

	ArrayList<Item> items = new ArrayList<Item>();
	SmolovJrAdapter smolovJrAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.activity_somolv_jr_cal);
		prefs = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		prefs_edit = prefs.edit();

		backButton = (ImageButton) findViewById(R.id.backButton);
		edittext1 = (EditText) findViewById(R.id.edittext1);
		edittext2 = (EditText) findViewById(R.id.edittext2);
		edittext1.setText(prefs.getString("oneRepJr", ""));
		edittext2.setText(prefs.getString("incrementJr", "5"));
		// if (Settings.reset_all2.equals("25")) {
		// prefs_edit.clear();
		// prefs_edit.commit();
		// Settings.reset_all2 = "0";
		// Toast.makeText(getApplicationContext(), "yes",
		// Toast.LENGTH_SHORT).show();
		// }

		textview1 = (TextView) findViewById(R.id.textview1);

		textday = (TextView) findViewById(R.id.textday);
		textsets = (TextView) findViewById(R.id.textsets);
		textreps = (TextView) findViewById(R.id.textreps);
		textweight = (TextView) findViewById(R.id.textweight);
		textdone = (TextView) findViewById(R.id.textdone);
		textonerep = (TextView) findViewById(R.id.textonerep);
		textincrement = (TextView) findViewById(R.id.textincrement);

		Typeface font1 = Typeface.createFromAsset(getAssets(),
				"fonts/Seravek.otf");

		textview1.setTypeface(font1);
		textday.setTypeface(font1);
		textsets.setTypeface(font1);
		textreps.setTypeface(font1);
		textweight.setTypeface(font1);
		textdone.setTypeface(font1);
		textonerep.setTypeface(font1);
		textincrement.setTypeface(font1);
		listview1 = (ListView) findViewById(R.id.listView1);

		items.add(new SectionHeader("Week 1"));
		items.add(new SmolovJr("1", "Day 1", "6", "6", "0.0", false));
		items.add(new SmolovJr("2", "Day 2", "7", "5", "0.0", false));
		items.add(new SmolovJr("3", "Day 3", "8", "4", "0.0", false));
		items.add(new SmolovJr("4", "Day 4", "10", "3", "0.0", false));
		items.add(new SectionHeader("Week 2"));
		items.add(new SmolovJr("1", "Day 1", "6", "6", "0.0", false));
		items.add(new SmolovJr("2", "Day 2", "7", "5", "0.0", false));
		items.add(new SmolovJr("3", "Day 3", "8", "4", "0.0", false));
		items.add(new SmolovJr("4", "Day 4", "10", "3", "0.0", false));
		items.add(new SectionHeader("Week 3"));
		items.add(new SmolovJr("1", "Day 1", "6", "6", "0.0", false));
		items.add(new SmolovJr("2", "Day 2", "7", "5", "0.0", false));
		items.add(new SmolovJr("3", "Day 3", "8", "4", "0.0", false));
		items.add(new SmolovJr("4", "Day 4", "10", "3", "0.0", false));

		smolovJrAdapter = new SmolovJrAdapter(SomolvJrCalActivity.this, items);
		listview1.setAdapter(smolovJrAdapter);

		edittext1.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
				txtTtitle4 = (TextView) findViewById(R.id.txtTitle4);

				edittext1_Text = edittext1.getText().toString();
				prefs_edit.putString("oneRepJr", edittext1_Text);
				edittext2_Text = edittext2.getText().toString();
				//Log.d("Viiiii", edittext2_Text);
				prefs_edit.putString("incrementJr", edittext2_Text);
				prefs_edit.commit();
				calcuations();
			}
		});
		edittext2.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(edittext2.getWindowToken(), 0);

			}
		});

		edittext2.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				txtTtitle4 = (TextView) findViewById(R.id.txtTitle4);
				edittext1_Text = edittext1.getText().toString();
				prefs_edit.putString("oneRepJr", edittext1_Text);
				edittext2_Text = edittext2.getText().toString();
				prefs_edit.putString("incrementJr", edittext2_Text);
				prefs_edit.commit();
				calcuations();
			}
		});

		listview1.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				
				  myview = arg2;

				if (arg2 != 0 && arg2 != 5 && arg2 != 10) {
					checkbox1 = (CheckBox) arg1.findViewById(R.id.checkbox1);
					// Toast.makeText(null, arg2, Toast.LENGTH_LONG).show();

					click_position = "" + arg2;
					Intent ep_MyIntent;

					if (checkbox1.isChecked()) {

						ep_MyIntent = new Intent(SomolvJrCalActivity.this,
								DayComplete.class);
						startActivity(ep_MyIntent);
						finish();
					} else if (!checkbox1.isChecked()) {
						ep_MyIntent = new Intent(SomolvJrCalActivity.this,
								GetAfterItActivity.class);
						startActivity(ep_MyIntent);
						finish();
					}

				}
			}
		});

		backButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		calcuations();
	}

	public void calcuations() {
		// *****************Calculations Started**FirstWeek***************

		edittext1_Text = edittext1.getText().toString();
		edittext2_Text = edittext2.getText().toString();

		if (edittext1_Text.length() != 0 && edittext2_Text.length() != 0) {

			int value1 = Integer.parseInt(edittext1_Text);
			int value2 = Integer.parseInt(edittext2_Text);

			txtTtitle1 = (TextView) findViewById(R.id.txtTitle1);

			float firstStep = 0;
			int secondStep = 0;
			float final_value1 = 0;

			// ********Calcualte Wt1**************

			firstStep = (float) ((value1 * 0.7) / 2.5);
			// firstStep = Math.round(firstStep);

			// float test = 1.4f;
			int abc = (int) firstStep;
			if (firstStep - 0.5 == abc) {
				secondStep = abc;
			} else {
				secondStep = Math.round(firstStep);
			}

			final_value1 = (float) (secondStep * 2.5);

			Wt1 = final_value1 + "";

			// ********Calcualte Wt2**************
			firstStep = (float) ((value1 * 0.75) / 2.5);

			abc = (int) firstStep;
			if (firstStep - 0.5 == abc) {
				secondStep = abc;
			} else {
				secondStep = Math.round(firstStep);
			}

			// firstStep = Math.round(firstStep);
			final_value1 = (float) (secondStep * 2.5);

			Wt2 = final_value1 + "";

			// txtTtitle4.setText(Wt3);

			// ********Calcualte Wt3**************

			firstStep = (float) ((value1 * 0.8) / 2.5);

			abc = (int) firstStep;
			if (firstStep - 0.5 == abc) {
				secondStep = abc;
			} else {
				secondStep = Math.round(firstStep);
			}
			// firstStep = Math.rint(1.5);
			final_value1 = (float) (secondStep * 2.5);
			Wt3 = final_value1 + "";

			// ********Calcualte Wt4**************

			firstStep = (float) ((value1 * 0.85) / 2.5);

			abc = (int) firstStep;
			if (firstStep - 0.5 == abc) {
				secondStep = abc;
			} else {
				secondStep = Math.round(firstStep);
			}
			// firstStep = Math.rint(1.5);
			final_value1 = (float) (secondStep * 2.5);
			Wt4 = final_value1 + "";

			// *****************Calculations Ended**FirstWeek***************

			// *****************Calculations Started**SecondWeek***************

			float Wt1_in_int = (float) Double.parseDouble(Wt1);
			float Wt2_in_int = (float) Double.parseDouble(Wt2);
			float Wt3_in_int = (float) Double.parseDouble(Wt3);
			float Wt4_in_int = (float) Double.parseDouble(Wt4);

			Wt5 = (Wt1_in_int + value2) + "";
			Wt6 = (Wt2_in_int + value2) + "";
			Wt7 = (Wt3_in_int + value2) + "";
			Wt8 = (Wt4_in_int + value2) + "";

			// *****************Calculations Started**SecondWeek***************

			Wt9 = (Wt1_in_int + (value2 * 2)) + "";
			Wt10 = (Wt2_in_int + (value2 * 2)) + "";
			Wt11 = (Wt3_in_int + (value2 * 2)) + "";
			Wt12 = (Wt4_in_int + (value2 * 2)) + "";

			((SmolovJr) items.get(1)).weight = Wt1;
			((SmolovJr) items.get(2)).weight = Wt2;
			((SmolovJr) items.get(3)).weight = Wt3;
			((SmolovJr) items.get(4)).weight = Wt4;
			// ((SmolovJr) items.get(5)).weight = Wt5;
			((SmolovJr) items.get(6)).weight = Wt5;
			((SmolovJr) items.get(7)).weight = Wt6;
			((SmolovJr) items.get(8)).weight = Wt7;
			((SmolovJr) items.get(9)).weight = Wt8;
			// ((SmolovJr) items.get(10)).weight = Wt10;
			((SmolovJr) items.get(11)).weight = Wt9;
			((SmolovJr) items.get(12)).weight = Wt10;
			((SmolovJr) items.get(13)).weight = Wt11;
			((SmolovJr) items.get(14)).weight = Wt12;

			smolovJrAdapter.notifyDataSetChanged();
		}

	}

	@Override
	public void onBackPressed() {
		startActivity(new Intent(SomolvJrCalActivity.this, MainActivity.class));
		 finish();
	}
}
