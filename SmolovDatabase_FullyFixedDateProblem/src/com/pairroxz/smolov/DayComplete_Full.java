package com.pairroxz.smolov;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class DayComplete_Full extends Activity {

	ImageView imageview1;
	TextView textview3, textview5, textview7, textview9, textview1, textview10,
			textview2, textview4, textview6, textview8;
	TextView textview0;
	ImageButton backButton;

	private SharedPreferences prefs;
	private SharedPreferences.Editor prefs_edit;

	@SuppressLint("SimpleDateFormat")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.activity_day_complete);

		prefs = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		prefs_edit = prefs.edit();

		imageview1 = (ImageView) findViewById(R.id.imageview1);
		textview0 = (TextView) findViewById(R.id.textview0);
		textview3 = (TextView) findViewById(R.id.textview3);
		textview5 = (TextView) findViewById(R.id.textview5);
		textview7 = (TextView) findViewById(R.id.textview7);
		textview9 = (TextView) findViewById(R.id.textview9);
		textview10 = (TextView) findViewById(R.id.textview10);
		textview1 = (TextView) findViewById(R.id.textview1);
		textview2 = (TextView) findViewById(R.id.textview2);
		textview4 = (TextView) findViewById(R.id.textview4);
		textview6 = (TextView) findViewById(R.id.textview6);
		textview8 = (TextView) findViewById(R.id.textview8);
		backButton = (ImageButton) findViewById(R.id.backButton);
		Typeface font1 = Typeface.createFromAsset(getAssets(),
				"fonts/Seravek.otf");

		textview0.setTypeface(font1);
		textview3.setTypeface(font1);
		textview5.setTypeface(font1);
		textview7.setTypeface(font1);
		textview9.setTypeface(font1);
		textview1.setTypeface(font1);
		textview10.setTypeface(font1);
		textview2.setTypeface(font1);
		textview4.setTypeface(font1);
		textview6.setTypeface(font1);
		textview8.setTypeface(font1);

		textview1.setText("You Crushed It !!");
		textview10.setText("Awesome Job !!");

		SimpleDateFormat take_date = new SimpleDateFormat("MM/dd/yyyy");
		String system_date = take_date.format(new Date());

		backButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				onBackPressed();

			}
		});

		// week 1
		if (SomolvFullActivity.click_position_full.equals("1")) {
			textview3.setText(SomolvFullActivity.wt1);
			textview0.setText("Day 1");
			textview5.setText("3");
			textview7.setText("8");
			if (prefs.getString("datef1", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef1", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef1", ""));
			} else {

				textview9.setText(prefs.getString("datef1", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("2")) {
			textview3.setText(SomolvFullActivity.wt2);
			textview0.setText("Day 1");
			textview5.setText("1");
			textview7.setText("5");
			if (prefs.getString("datef2", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef2", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef2", ""));
			} else {

				textview9.setText(prefs.getString("datef2", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("3")) {
			textview3.setText(SomolvFullActivity.wt3);
			textview0.setText("Day 1");
			textview5.setText("2");
			textview7.setText("2");
			if (prefs.getString("datef3", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef3", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef3", ""));
			} else {

				textview9.setText(prefs.getString("datef3", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("4")) {
			textview3.setText(SomolvFullActivity.wt4);
			textview0.setText("Day 1");
			textview5.setText("1");
			textview7.setText("1");
			if (prefs.getString("datef4", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef4", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef4", ""));
			} else {

				textview9.setText(prefs.getString("datef4", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("5")) {
			textview3.setText(SomolvFullActivity.wt1);
			textview0.setText("Day 2");
			textview5.setText("3");
			textview7.setText("8");
			if (prefs.getString("datef5", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef5", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef5", ""));
			} else {

				textview9.setText(prefs.getString("datef5", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("6")) {
			textview3.setText(SomolvFullActivity.wt2);
			textview0.setText("Day 2");
			textview5.setText("1");
			textview7.setText("5");
			if (prefs.getString("datef6", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef6", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef6", ""));
			} else {

				textview9.setText(prefs.getString("datef6", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("7")) {
			textview3.setText(SomolvFullActivity.wt3);
			textview0.setText("Day 2");
			textview5.setText("2");
			textview7.setText("2");
			if (prefs.getString("datef7", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef7", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef7", ""));
			} else {

				textview9.setText(prefs.getString("datef7", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("8")) {
			textview3.setText(SomolvFullActivity.wt4);
			textview0.setText("Day 2");
			textview5.setText("1");
			textview7.setText("1");
			if (prefs.getString("datef8", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef8", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef8", ""));
			} else {

				textview9.setText(prefs.getString("datef8", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("9")) {
			textview3.setText(SomolvFullActivity.wt2);
			textview0.setText("Day 3");
			textview5.setText("4");
			textview7.setText("5");
			if (prefs.getString("datef9", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef9", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef9", ""));
			} else {

				textview9.setText(prefs.getString("datef9", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("10")) {
			textview3.setText(SomolvFullActivity.wt4);
			textview0.setText("Day 3");
			textview5.setText("1");
			textview7.setText("3");
			if (prefs.getString("datef10", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef10", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef10", ""));
			} else {

				textview9.setText(prefs.getString("datef10", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("11")) {
			textview3.setText(SomolvFullActivity.wt6);
			textview0.setText("Day 3");
			textview5.setText("2");
			textview7.setText("2");
			if (prefs.getString("datef11", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef11", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef11", ""));
			} else {

				textview9.setText(prefs.getString("datef11", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("12")) {
			textview3.setText(SomolvFullActivity.wt5);
			textview0.setText("Day 3");
			textview5.setText("1");
			textview7.setText("1");
			if (prefs.getString("datef12", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef12", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef12", ""));
			} else {

				textview9.setText(prefs.getString("datef12", ""));
			}

		}
		// week 2
		else if (SomolvFullActivity.click_position_full.equals("14")) {
			textview3.setText(SomolvFullActivity.wt2);
			textview0.setText("Day 1");
			textview5.setText("1");
			textview7.setText("5");
			if (prefs.getString("datef14", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef14", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef14", ""));
			} else {

				textview9.setText(prefs.getString("datef14", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("15")) {
			textview3.setText(SomolvFullActivity.wt3);
			textview0.setText("Day 2");
			textview5.setText("1");
			textview7.setText("5");
			if (prefs.getString("datef15", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef15", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef15", ""));
			} else {

				textview9.setText(prefs.getString("datef15", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("16")) {
			textview3.setText(SomolvFullActivity.wt4);
			textview0.setText("Day 3");
			textview5.setText("1");
			textview7.setText("5");
			if (prefs.getString("datef16", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef16", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef16", ""));
			} else {

				textview9.setText(prefs.getString("datef16", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("18")) {
			textview3.setText(SomolvFullActivity.wt7);
			textview0.setText("Day 1");
			textview5.setText("4");
			textview7.setText("9");
			if (prefs.getString("datef18", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef18", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef18", ""));
			} else {

				textview9.setText(prefs.getString("datef18", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("19")) {
			textview3.setText(SomolvFullActivity.wt8);
			textview0.setText("Day 2");
			textview5.setText("5");
			textview7.setText("7");
			if (prefs.getString("datef19", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef19", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef19", ""));
			} else {

				textview9.setText(prefs.getString("datef19", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("20")) {
			textview3.setText(SomolvFullActivity.wt9);
			textview0.setText("Day 3");
			textview5.setText("7");
			textview7.setText("5");
			if (prefs.getString("datef20", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef20", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef20", ""));
			} else {

				textview9.setText(prefs.getString("datef20", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("21")) {
			textview3.setText(SomolvFullActivity.wt10);
			textview0.setText("Day 4");
			textview5.setText("10");
			textview7.setText("3");
			if (prefs.getString("datef21", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef21", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef21", ""));
			} else {

				textview9.setText(prefs.getString("datef21", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("23")) {
			textview3.setText(SomolvFullActivity.wt11);
			textview0.setText("Day 1");
			textview5.setText("4");
			textview7.setText("9");
			if (prefs.getString("datef23", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef23", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef23", ""));
			} else {

				textview9.setText(prefs.getString("datef23", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("24")) {
			textview3.setText(SomolvFullActivity.wt12);
			textview0.setText("Day 2");
			textview5.setText("5");
			textview7.setText("7");
			if (prefs.getString("datef24", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef24", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef24", ""));
			} else {

				textview9.setText(prefs.getString("datef24", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("25")) {
			textview3.setText(SomolvFullActivity.wt13);
			textview0.setText("Day 3");
			textview5.setText("7");
			textview7.setText("5");
			if (prefs.getString("datef25", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef25", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef25", ""));
			} else {

				textview9.setText(prefs.getString("datef25", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("26")) {
			textview3.setText(SomolvFullActivity.wt14);
			textview0.setText("Day 4");
			textview5.setText("10");
			textview7.setText("3");
			if (prefs.getString("datef26", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef26", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef26", ""));
			} else {

				textview9.setText(prefs.getString("datef26", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("28")) {
			textview3.setText(SomolvFullActivity.wt15);
			textview0.setText("Day 1");
			textview5.setText("4");
			textview7.setText("9");
			if (prefs.getString("datef28", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef28", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef28", ""));
			} else {

				textview9.setText(prefs.getString("datef28", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("29")) {
			textview3.setText(SomolvFullActivity.wt1);
			textview0.setText("Day 2");
			textview5.setText("5");
			textview7.setText("7");
			if (prefs.getString("datef29", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef29", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef29", ""));
			} else {

				textview9.setText(prefs.getString("datef29", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("30")) {
			textview3.setText(SomolvFullActivity.wt3);
			textview0.setText("Day 3");
			textview5.setText("7");
			textview7.setText("5");
			if (prefs.getString("datef30", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef30", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef30", ""));
			} else {

				textview9.setText(prefs.getString("datef30", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("31")) {
			textview3.setText(SomolvFullActivity.wt7);
			textview0.setText("Day 4");
			textview5.setText("10");
			textview7.setText("3");
			if (prefs.getString("datef31", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef31", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef31", ""));
			} else {

				textview9.setText(prefs.getString("datef31", ""));
			}

		}
		// week 6
		else if (SomolvFullActivity.click_position_full.equals("33")) {
			textview3.setText(SomolvFullActivity.wt7);
			textview5.setText("0");
			textview7.setText("0");
			textview0.setText("Day 1");
			if (prefs.getString("datef33", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef33", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef33", ""));
			} else {

				textview9.setText(prefs.getString("datef33", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("34")) {
			textview3.setText(SomolvFullActivity.wt0);
			textview5.setText("0");
			textview7.setText("0");
			textview0.setText("Day 2");
			if (prefs.getString("datef34", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef34", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef34", ""));
			} else {

				textview9.setText(prefs.getString("datef34", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("35")) {
			textview3.setText(SomolvFullActivity.wt2);
			textview5.setText("1");
			textview7.setText("1");
			textview0.setText("Day 3");
			if (prefs.getString("datef35", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef35", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef35", ""));
			} else {

				textview9.setText(prefs.getString("datef35", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("36")) {
			textview3.setText(SomolvFullActivity.wt4);
			textview5.setText("1");
			textview7.setText("1");
			textview0.setText("Day 4");
			if (prefs.getString("datef36", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef36", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef36", ""));
			} else {

				textview9.setText(prefs.getString("datef36", ""));
			}

		}
		// week 9 day 1
		else if (SomolvFullActivity.click_position_full.equals("42")) {
			textview3.setText(SomolvFullActivity.wt5);
			textview0.setText("Day 1");
			textview5.setText("1");
			textview7.setText("3");
			if (prefs.getString("datef42", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef42", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef42", ""));
			} else {

				textview9.setText(prefs.getString("datef42", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("43")) {
			textview3.setText(SomolvFullActivity.wt7);
			textview0.setText("Day 1");
			textview5.setText("1");
			textview7.setText("4");
			if (prefs.getString("datef43", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef43", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef43", ""));
			} else {

				textview9.setText(prefs.getString("datef43", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("44")) {
			textview3.setText(SomolvFullActivity.wt1);
			textview0.setText("Day 1");
			textview5.setText("3");
			textview7.setText("4");
			if (prefs.getString("datef44", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef44", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef44", ""));
			} else {

				textview9.setText(prefs.getString("datef44", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("45")) {
			textview3.setText(SomolvFullActivity.wt2);
			textview5.setText("1");
			textview0.setText("Day 1");
			textview7.setText("5");
			if (prefs.getString("datef45", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef45", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef45", ""));
			} else {

				textview9.setText(prefs.getString("datef45", ""));
			}

		}
		// week 9 day 2
		else if (SomolvFullActivity.click_position_full.equals("46")) {
			textview3.setText(SomolvFullActivity.wt4);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 2");
			if (prefs.getString("datef46", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef46", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef46", ""));
			} else {

				textview9.setText(prefs.getString("datef46", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("47")) {
			textview3.setText(SomolvFullActivity.wt0);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 2");
			if (prefs.getString("datef47", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef47", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef47", ""));
			} else {

				textview9.setText(prefs.getString("datef47", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("48")) {
			textview3.setText(SomolvFullActivity.wt2);
			textview5.setText("1");
			textview7.setText("4");
			textview0.setText("Day 2");
			if (prefs.getString("datef48", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef48", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef48", ""));
			} else {

				textview9.setText(prefs.getString("datef48", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("49")) {
			textview3.setText(SomolvFullActivity.wt4);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 2");
			if (prefs.getString("datef49", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef49", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef49", ""));
			} else {

				textview9.setText(prefs.getString("datef49", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("50")) {
			textview3.setText(SomolvFullActivity.wt5);
			textview5.setText("2");
			textview7.setText("5");
			textview0.setText("Day 2");
			if (prefs.getString("datef50", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef50", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef50", ""));
			} else {

				textview9.setText(prefs.getString("datef50", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("51")) {
			textview3.setText(SomolvFullActivity.wt7);
			textview5.setText("1");
			textview7.setText("4");
			textview0.setText("Day 3");
			if (prefs.getString("datef51", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef51", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef51", ""));
			} else {

				textview9.setText(prefs.getString("datef51", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("52")) {
			textview3.setText(SomolvFullActivity.wt1);
			textview5.setText("1");
			textview7.setText("4");
			textview0.setText("Day 3");
			if (prefs.getString("datef52", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef52", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef52", ""));
			} else {

				textview9.setText(prefs.getString("datef52", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("53")) {
			textview3.setText(SomolvFullActivity.wt2);
			textview5.setText("5");
			textview7.setText("4");
			textview0.setText("Day 3");
			if (prefs.getString("datef53", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef53", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef53", ""));
			} else {

				textview9.setText(prefs.getString("datef53", ""));
			}

		}

		// week 10
		else if (SomolvFullActivity.click_position_full.equals("55")) {
			textview3.setText(SomolvFullActivity.wt4);
			textview5.setText("1");
			textview7.setText("4");
			textview0.setText("Day 1");
			if (prefs.getString("datef55", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef55", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef55", ""));
			} else {

				textview9.setText(prefs.getString("datef55", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("56")) {
			textview3.setText(SomolvFullActivity.wt0);
			textview5.setText("1");
			textview7.setText("4");
			textview0.setText("Day 1");
			if (prefs.getString("datef56", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef56", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef56", ""));
			} else {

				textview9.setText(prefs.getString("datef56", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("57")) {
			textview3.setText(SomolvFullActivity.wt2);
			textview5.setText("1");
			textview7.setText("4");
			textview0.setText("Day 1");
			if (prefs.getString("datef57", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef57", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef57", ""));
			} else {

				textview9.setText(prefs.getString("datef57", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("58")) {
			textview3.setText(SomolvFullActivity.wt4);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 1");
			if (prefs.getString("datef58", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef58", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef58", ""));
			} else {

				textview9.setText(prefs.getString("datef58", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("59")) {
			textview3.setText(SomolvFullActivity.wt5);
			textview5.setText("2");
			textview7.setText("4");
			textview0.setText("Day 1");
			if (prefs.getString("datef59", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef59", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef59", ""));
			} else {

				textview9.setText(prefs.getString("datef59", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("60")) {
			textview3.setText(SomolvFullActivity.wt5);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 2");
			if (prefs.getString("datef60", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef60", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef60", ""));
			} else {

				textview9.setText(prefs.getString("datef60", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("61")) {
			textview3.setText(SomolvFullActivity.wt1);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 2");
			if (prefs.getString("datef61", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef61", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef61", ""));
			} else {

				textview9.setText(prefs.getString("datef61", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("62")) {
			textview3.setText(SomolvFullActivity.wt3);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 2");
			if (prefs.getString("datef62", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef62", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef62", ""));
			} else {

				textview9.setText(prefs.getString("datef62", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("63")) {
			textview3.setText(SomolvFullActivity.wt7);
			textview5.setText("3");
			textview7.setText("3");
			textview0.setText("Day 2");
			if (prefs.getString("datef63", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef63", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef63", ""));
			} else {

				textview9.setText(prefs.getString("datef63", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("64")) {
			textview3.setText(SomolvFullActivity.wt5);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 2");
			if (prefs.getString("datef64", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef64", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef64", ""));
			} else {

				textview9.setText(prefs.getString("datef64", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("65")) {
			textview3.setText(SomolvFullActivity.wt16);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 3");
			if (prefs.getString("datef65", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef65", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef65", ""));
			} else {

				textview9.setText(prefs.getString("datef65", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("66")) {
			textview3.setText(SomolvFullActivity.wt1);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 3");
			if (prefs.getString("datef66", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef66", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef66", ""));
			} else {

				textview9.setText(prefs.getString("datef66", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("67")) {
			textview3.setText(SomolvFullActivity.wt3);
			textview5.setText("1");
			textview7.setText("4");
			textview0.setText("Day 3");
			if (prefs.getString("datef67", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef67", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef67", ""));
			} else {

				textview9.setText(prefs.getString("datef67", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("68")) {
			textview3.setText(SomolvFullActivity.wt7);
			textview5.setText("4");
			textview7.setText("5");
			textview0.setText("Day 3");
			if (prefs.getString("datef68", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef68", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef68", ""));
			} else {

				textview9.setText(prefs.getString("datef68", ""));
			}

		}
		// week 11
		else if (SomolvFullActivity.click_position_full.equals("70")) {
			textview3.setText(SomolvFullActivity.wt5);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 1");
			if (prefs.getString("datef70", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef70", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef70", ""));
			} else {

				textview9.setText(prefs.getString("datef70", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("71")) {
			textview3.setText(SomolvFullActivity.wt0);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 1");
			if (prefs.getString("datef71", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef71", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef71", ""));
			} else {

				textview9.setText(prefs.getString("datef71", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("72")) {
			textview3.setText(SomolvFullActivity.wt2);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 1");
			if (prefs.getString("datef72", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef72", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef72", ""));
			} else {

				textview9.setText(prefs.getString("datef72", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("73")) {
			textview3.setText(SomolvFullActivity.wt4);
			textview5.setText("5");
			textview7.setText("5");
			textview0.setText("Day 1");
			if (prefs.getString("datef73", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef73", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef73", ""));
			} else {

				textview9.setText(prefs.getString("datef73", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("74")) {
			textview3.setText(SomolvFullActivity.wt5);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 2");
			if (prefs.getString("datef74", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef74", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef74", ""));
			} else {

				textview9.setText(prefs.getString("datef74", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("75")) {
			textview3.setText(SomolvFullActivity.wt2);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 2");
			if (prefs.getString("datef75", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef75", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef75", ""));
			} else {

				textview9.setText(prefs.getString("datef75", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("76")) {
			textview3.setText(SomolvFullActivity.wt4);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 2");
			if (prefs.getString("datef76", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef76", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef76", ""));
			} else {

				textview9.setText(prefs.getString("datef76", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("77")) {
			textview3.setText(SomolvFullActivity.wt16);
			textview5.setText("2");
			textview7.setText("3");
			textview0.setText("Day 2");
			if (prefs.getString("datef77", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef77", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef77", ""));
			} else {

				textview9.setText(prefs.getString("datef77", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("78")) {
			textview3.setText(SomolvFullActivity.wt1);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 3");
			if (prefs.getString("datef78", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef78", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef78", ""));
			} else {

				textview9.setText(prefs.getString("datef78", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("79")) {
			textview3.setText(SomolvFullActivity.wt3);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 3");
			if (prefs.getString("datef79", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef79", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef79", ""));
			} else {

				textview9.setText(prefs.getString("datef79", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("80")) {
			textview3.setText(SomolvFullActivity.wt7);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 3");
			if (prefs.getString("datef80", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef80", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef80", ""));
			} else {

				textview9.setText(prefs.getString("datef80", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("81")) {
			textview3.setText(SomolvFullActivity.wt16);
			textview5.setText("4");
			textview7.setText("3");
			textview0.setText("Day 3");
			if (prefs.getString("datef81", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef81", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef81", ""));
			} else {

				textview9.setText(prefs.getString("datef81", ""));
			}

		}
		// week 12
		else if (SomolvFullActivity.click_position_full.equals("83")) {
			textview3.setText(SomolvFullActivity.wt2);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 1");
			if (prefs.getString("datef83", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef83", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef83", ""));
			} else {

				textview9.setText(prefs.getString("datef83", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("84")) {
			textview3.setText(SomolvFullActivity.wt4);
			textview5.setText("1");
			textview7.setText("4");
			textview0.setText("Day 1");
			if (prefs.getString("datef84", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef84", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef84", ""));
			} else {

				textview9.setText(prefs.getString("datef84", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("85")) {
			textview3.setText(SomolvFullActivity.wt5);
			textview5.setText("5");
			textview7.setText("5");
			textview0.setText("Day 1");
			if (prefs.getString("datef85", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef85", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef85", ""));
			} else {

				textview9.setText(prefs.getString("datef85", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("86")) {
			textview3.setText(SomolvFullActivity.wt2);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 2");
			if (prefs.getString("datef86", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef86", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef86", ""));
			} else {

				textview9.setText(prefs.getString("datef86", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("87")) {
			textview3.setText(SomolvFullActivity.wt4);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 2");
			if (prefs.getString("datef87", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef87", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef87", ""));
			} else {

				textview9.setText(prefs.getString("datef87", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("88")) {
			textview3.setText(SomolvFullActivity.wt16);
			textview5.setText("4");
			textview7.setText("3");
			textview0.setText("Day 2");
			if (prefs.getString("datef88", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef88", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef88", ""));
			} else {

				textview9.setText(prefs.getString("datef88", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("89")) {
			textview3.setText(SomolvFullActivity.wt3);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 3");
			if (prefs.getString("datef89", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef89", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef89", ""));
			} else {

				textview9.setText(prefs.getString("datef89", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("90")) {
			textview3.setText(SomolvFullActivity.wt4);
			textview5.setText("1");
			textview7.setText("4");
			textview0.setText("Day 3");
			if (prefs.getString("datef90", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef90", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef90", ""));
			} else {

				textview9.setText(prefs.getString("datef90", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("91")) {
			textview3.setText(SomolvFullActivity.wt5);
			textview5.setText("3");
			textview7.setText("4");
			textview0.setText("Day 3");
			if (prefs.getString("datef91", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef91", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef91", ""));
			} else {

				textview9.setText(prefs.getString("datef91", ""));
			}

		}
		// Tapper Week
		else if (SomolvFullActivity.click_position_full.equals("93")) {
			textview3.setText(SomolvFullActivity.wt2);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 1");
			if (prefs.getString("datef93", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef93", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef93", ""));
			} else {

				textview9.setText(prefs.getString("datef93", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("94")) {
			textview3.setText(SomolvFullActivity.wt4);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 1");
			if (prefs.getString("datef94", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef94", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef94", ""));
			} else {

				textview9.setText(prefs.getString("datef94", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("95")) {
			textview3.setText(SomolvFullActivity.wt5);
			textview5.setText("2");
			textview7.setText("5");
			textview0.setText("Day 1");
			if (prefs.getString("datef95", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef95", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef95", ""));
			} else {

				textview9.setText(prefs.getString("datef95", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("96")) {
			textview3.setText(SomolvFullActivity.wt16);
			textview5.setText("3");
			textview7.setText("4");
			textview0.setText("Day 1");
			if (prefs.getString("datef96", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef96", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef96", ""));
			} else {

				textview9.setText(prefs.getString("datef96", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("97")) {
			textview3.setText(SomolvFullActivity.wt2);
			textview5.setText("1");
			textview7.setText("4");
			textview0.setText("Day 2");
			if (prefs.getString("datef97", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef97", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef97", ""));
			} else {

				textview9.setText(prefs.getString("datef97", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("98")) {
			textview3.setText(SomolvFullActivity.wt4);
			textview5.setText("4");
			textview7.setText("4");
			textview0.setText("Day 2");
			if (prefs.getString("datef98", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef98", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef98", ""));
			} else {

				textview9.setText(prefs.getString("datef98", ""));
			}

		} else if (SomolvFullActivity.click_position_full.equals("99")) {
			textview3.setText(SomolvFullActivity.wt4);
			textview5.setText("1");
			textview7.setText("1");
			textview0.setText("Day 3");
			if (prefs.getString("datef99", "").equalsIgnoreCase("")) {
				prefs_edit.putString("datef99", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("datef99", ""));
			} else {

				textview9.setText(prefs.getString("datef99", ""));
			}

		}

	}

	@Override
	public void onBackPressed() {
		// Intent myIntent = new Intent(DayComplete_Full.this,
		// SomolvFullActivity.class);
		// startActivity(myIntent);
		finish();
	}
}
