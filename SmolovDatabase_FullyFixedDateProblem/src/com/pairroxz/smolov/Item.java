package com.pairroxz.smolov;

public interface Item {
	public boolean isSection();
	public boolean isURL();
}
