package com.pairroxz.smolov;

public class SmolovFull implements Item{
	public String id;
	public String day;
	public String lift;
	public String sets;
	public String reps;
	public String weight;
	public boolean done;
	
	public SmolovFull(String id,String day,String lift,String sets,String reps,String weight,boolean done) {
		this.id=id;
		this.day=day;
		this.lift=lift;
		this.sets=sets;
		this.reps=reps;
		this.weight=weight;
		this.done=done;
	}
	
	@Override
	public boolean isSection() {
		return false;
	}

	@Override
	public boolean isURL() {
		return false;
	}

}
