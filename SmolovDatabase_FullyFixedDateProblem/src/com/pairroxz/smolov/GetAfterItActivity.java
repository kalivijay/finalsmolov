package com.pairroxz.smolov;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class GetAfterItActivity extends Activity {

	TextView textview3, textview5, textview7, textview9, textview1, textview2,
			textview4, textview6, textview8;
	TextView textview0;
	ImageButton backButton;
	private SharedPreferences prefs;
	private SharedPreferences.Editor prefs_edit;

	@SuppressLint("SimpleDateFormat")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.activity_get_after_it);
		Typeface font1 = Typeface.createFromAsset(getAssets(),
				"fonts/Seravek.otf");

		prefs = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		prefs_edit = prefs.edit();

		textview0 = (TextView) findViewById(R.id.textview0);
		textview3 = (TextView) findViewById(R.id.textview3);
		textview5 = (TextView) findViewById(R.id.textview5);
		textview7 = (TextView) findViewById(R.id.textview7);
		textview9 = (TextView) findViewById(R.id.textview9);
		textview1 = (TextView) findViewById(R.id.textview1);
		textview2 = (TextView) findViewById(R.id.textview2);
		textview4 = (TextView) findViewById(R.id.textview4);
		textview6 = (TextView) findViewById(R.id.textview6);
		textview8 = (TextView) findViewById(R.id.textview8);
		backButton = (ImageButton) findViewById(R.id.backButton);
		textview0.setTypeface(font1);
		textview3.setTypeface(font1);
		textview5.setTypeface(font1);
		textview7.setTypeface(font1);
		textview9.setTypeface(font1);
		textview1.setTypeface(font1);
		textview2.setTypeface(font1);
		textview4.setTypeface(font1);
		textview6.setTypeface(font1);
		textview8.setTypeface(font1);

		textview1.setText("Get After It !");

		/*
		 * SimpleDateFormat take_date = new SimpleDateFormat("MM/dd/yyyy");
		 * String system_date = take_date.format(new Date());
		 * textview9.setText(system_date);
		 */

		backButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				onBackPressed();
			}
		});

		if (SomolvJrCalActivity.click_position.equals("1")) {
			textview3.setText(SomolvJrCalActivity.Wt1);
			textview0.setText("Day 1");
			textview5.setText("6");
			textview7.setText("6");

			//Put nothing/blank in prefs 
			prefs_edit.putString("date1", "");
			prefs_edit.commit();

		} else if (SomolvJrCalActivity.click_position.equals("2")) {
			textview3.setText(SomolvJrCalActivity.Wt2);
			textview0.setText("Day 2");
			textview5.setText("7");
			textview7.setText("5");
			prefs_edit.putString("date2", "");
			prefs_edit.commit();

		} else if (SomolvJrCalActivity.click_position.equals("3")) {
			textview3.setText(SomolvJrCalActivity.Wt3);
			textview0.setText("Day 3");
			textview5.setText("8");
			textview7.setText("4");
			prefs_edit.putString("date3", "");
			prefs_edit.commit();
		} else if (SomolvJrCalActivity.click_position.equals("4")) {
			textview3.setText(SomolvJrCalActivity.Wt4);
			textview0.setText("Day 4");
			textview5.setText("10");
			textview7.setText("3");
			prefs_edit.putString("date4", "");
			prefs_edit.commit();

		} else if (SomolvJrCalActivity.click_position.equals("6")) {
			textview3.setText(SomolvJrCalActivity.Wt5);
			textview0.setText("Day 1");
			textview5.setText("6");
			textview7.setText("6");
			prefs_edit.putString("date5", "");
			prefs_edit.commit();
		} else if (SomolvJrCalActivity.click_position.equals("7")) {
			textview3.setText(SomolvJrCalActivity.Wt6);
			textview0.setText("Day 2");
			textview5.setText("7");
			textview7.setText("5");
			prefs_edit.putString("date6", "");
			prefs_edit.commit();
		} else if (SomolvJrCalActivity.click_position.equals("8")) {
			textview3.setText(SomolvJrCalActivity.Wt7);
			textview0.setText("Day 3");
			textview5.setText("8");
			textview7.setText("4");
			prefs_edit.putString("date7", "");
			prefs_edit.commit();
		} else if (SomolvJrCalActivity.click_position.equals("9")) {
			textview3.setText(SomolvJrCalActivity.Wt8);
			textview0.setText("Day 4");
			textview5.setText("10");
			textview7.setText("3");
			prefs_edit.putString("date8", "");
			prefs_edit.commit();
		} else if (SomolvJrCalActivity.click_position.equals("11")) {
			textview3.setText(SomolvJrCalActivity.Wt9);
			textview0.setText("Day 1");
			textview5.setText("6");
			textview7.setText("6");
			prefs_edit.putString("date9", "");
			prefs_edit.commit();
		} else if (SomolvJrCalActivity.click_position.equals("12")) {
			textview3.setText(SomolvJrCalActivity.Wt10);
			textview0.setText("Day 2");
			textview5.setText("7");
			textview7.setText("5");
			prefs_edit.putString("date10", "");
			prefs_edit.commit();
		} else if (SomolvJrCalActivity.click_position.equals("13")) {
			textview3.setText(SomolvJrCalActivity.Wt11);
			textview0.setText("Day 3");
			textview5.setText("8");
			textview7.setText("4");
			prefs_edit.putString("date11", "");
			prefs_edit.commit();
		} else if (SomolvJrCalActivity.click_position.equals("14")) {
			textview3.setText(SomolvJrCalActivity.Wt12);
			textview0.setText("Day 4");
			textview5.setText("10");
			textview7.setText("3");
			prefs_edit.putString("date12", "");
			prefs_edit.commit();
		}

	}

	@Override
	public void onBackPressed() {
		Intent myIntent = new Intent(GetAfterItActivity.this,
				SomolvJrCalActivity.class);
		startActivity(myIntent);
		finish();
	}
}
