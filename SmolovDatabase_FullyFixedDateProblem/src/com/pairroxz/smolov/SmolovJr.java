package com.pairroxz.smolov;

public class SmolovJr implements Item {
	public String id;
	public String day;

	public String sets;
	public String reps;
	public String weight;
	public boolean done;

	public SmolovJr(String id, String day, String sets, String reps,
			String weight, boolean done) {
		this.id = id;
		this.day = day;

		this.sets = sets;
		this.reps = reps;
		this.weight = weight;
		this.done = done;
	}

	@Override
	public boolean isSection() {
		return false;
	}

	@Override
	public boolean isURL() {
		// TODO Auto-generated method stub
		return false;
	}

}
