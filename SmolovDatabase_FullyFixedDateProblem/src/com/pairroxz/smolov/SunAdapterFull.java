package com.pairroxz.smolov;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

public class SunAdapterFull extends ArrayAdapter<SunFull> {

	Context context;
	int layoutResourceId;
	SunFull data[] = null;
	public SharedPreferences check_status;
	public SharedPreferences.Editor check_status_edit;
	String loginPrefs = "hi";
	boolean check_value;
	ArrayList list, list2;
	int i = 0;

	public SunAdapterFull(Context context, int layoutResourceId,
			SunFull[] data, ArrayList list, ArrayList list2) {
		super(context, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
		this.list = list;
		this.list2 = list2;
		check_status = context.getSharedPreferences(loginPrefs,
				Context.MODE_PRIVATE);
		check_status_edit = check_status.edit();
		if (Settings.reset_all.equals("25")) {
			check_status_edit.clear();
			check_status_edit.commit();
			Settings.reset_all = "0";
		}
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View row = convertView;
		WeatherHolder holder = null;
		SectionHolder Sholder = null;
		SectionHolder2 Sholder_of_2 = null;
		final SunFull weather = data[position];

		Toast.makeText(getContext(), "afd:asdfasfd " + i, Toast.LENGTH_SHORT)
				.show();
		i++;

		LayoutInflater inflater = ((Activity) context).getLayoutInflater();
		row = inflater.inflate(R.layout.listview_secton, parent, false);
		Sholder = new SectionHolder(row);

		if (position == 0) {
			Sholder.SectionText.setText(" Week 1 - Phase In");
		} else if (position == 13) {
			Sholder.SectionText.setText(" Week 2 - Phase In");
		} else if (position == 17) {
			Sholder.SectionText.setText(" Week - 3 Base Cycle");
		} else if (position == 22) {
			Sholder.SectionText.setText(" Week - 4 Base Cycle");
		} else if (position == 27) {
			Sholder.SectionText.setText(" Week - 5 Base Cycle");
		} else if (position == 32) {
			Sholder.SectionText.setText(" Week - 6 Base Cycle");
		} else if (position == 37) {
			Sholder.SectionText.setText(" Week - 7 Switching Cycle");
		} else if (position == 39) {
			Sholder.SectionText.setText(" Week - 8 Switching Cycle");
		} else if (position == 41) {
			Sholder.SectionText.setText(" Week - 9 Intense Cycle");
		} else if (position == 54) {
			Sholder.SectionText.setText(" Week - 10 Intense Cycle");
		} else if (position == 69) {
			Sholder.SectionText.setText(" Week - 11 Intense Cycle");
		} else if (position == 82) {
			Sholder.SectionText.setText(" Week - 12 Intense Cycle");
		} else if (position == 92) {
			Sholder.SectionText.setText(" Week - 13 Tapper Week");
			Toast.makeText(getContext(), "adfs", Toast.LENGTH_SHORT).show();
		}

		else if (position == 38 || position == 40) {
			LayoutInflater inflater1 = ((Activity) context).getLayoutInflater();
			row = inflater1.inflate(R.layout.textview_smolov, parent, false);
			Sholder_of_2 = new SectionHolder2(row);

			if (position == 38) {
				Sholder_of_2.SectionText2.setText("See smolovjr.com for details");
			}
			if (position == 40) {
				Sholder_of_2.SectionText2.setText("See smolovjr.com for details");
			}
			// Sholder_of_2.SectionText2.setTypeface(font1);
		}

		else {
			LayoutInflater inflater2 = ((Activity) context).getLayoutInflater();
			row = inflater2.inflate(layoutResourceId, parent, false);
			holder = new WeatherHolder(row);

			// holder.txtTitle1.setTypeface(font1);
			// holder.txtTitle2.setTypeface(font1);
			// holder.txtTitle3.setTypeface(font1);
			// holder.txtTitle4.setTypeface(font1);
			holder.txtTitle1.setText(weather.title1);
			holder.txtTitle2.setText(weather.title2);
			holder.txtTitle3.setText(weather.title3);
			holder.txtTitle4.setText(weather.title4);
			holder.txtTitle5.setText("  " + list.get(position).toString());

			notifyDataSetChanged();

			check_value = check_status.getBoolean(weather.peh_id, false);

			holder.checkbox1.setChecked(check_value);
			holder.checkbox1
					.setOnCheckedChangeListener(new OnCheckedChangeListener() {

						public void onCheckedChanged(CompoundButton arg0,
								boolean arg1) {
							// TODO Auto-generated method stub

							check_status_edit.putBoolean(weather.peh_id, arg1);
							check_status_edit.commit();

						}

					});

			notifyDataSetChanged();
		}

		return row;
	}

	static class WeatherHolder {
		// ImageView imgIcon;
		TextView txtTitle1, txtTitle2, txtTitle3, txtTitle4, txtTitle5;
		CheckBox checkbox1;

		public WeatherHolder(View view) {
			txtTitle1 = (TextView) view.findViewById(R.id.txtTitle1);
			txtTitle2 = (TextView) view.findViewById(R.id.txtTitle2);
			txtTitle3 = (TextView) view.findViewById(R.id.txtTitle3);
			txtTitle4 = (TextView) view.findViewById(R.id.txtTitle4);
			txtTitle5 = (TextView) view.findViewById(R.id.txtTitle5);
			checkbox1 = (CheckBox) view.findViewById(R.id.checkbox1);
		}

	}

	class SectionHolder {
		TextView SectionText;

		public SectionHolder(View view) {
			SectionText = (TextView) view.findViewById(R.id.txtHeader);
		}
	}

	class SectionHolder2 {
		TextView SectionText2;

		public SectionHolder2(View view) {
			SectionText2 = (TextView) view.findViewById(R.id.textview_for_web);
		}
	}

}
