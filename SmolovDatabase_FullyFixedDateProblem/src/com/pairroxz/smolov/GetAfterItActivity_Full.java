package com.pairroxz.smolov;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

public class GetAfterItActivity_Full extends Activity {

	TextView textview3, textview5, textview7, textview9, textview1, textview2,
			textview4, textview6, textview8;
	TextView textview0;
	ImageButton backButton;

	@SuppressLint("SimpleDateFormat")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		Typeface font1 = Typeface.createFromAsset(getAssets(),
				"fonts/Seravek.otf");

		setContentView(R.layout.activity_get_after_it);

		textview0 = (TextView) findViewById(R.id.textview0);
		textview3 = (TextView) findViewById(R.id.textview3);
		textview5 = (TextView) findViewById(R.id.textview5);
		textview7 = (TextView) findViewById(R.id.textview7);
		textview9 = (TextView) findViewById(R.id.textview9);
		textview2 = (TextView) findViewById(R.id.textview2);
		textview4 = (TextView) findViewById(R.id.textview4);
		textview6 = (TextView) findViewById(R.id.textview6);
		textview8 = (TextView) findViewById(R.id.textview8);
		textview9 = (TextView) findViewById(R.id.textview9);
		textview1 = (TextView) findViewById(R.id.textview1);
		backButton = (ImageButton) findViewById(R.id.backButton);

		textview0.setTypeface(font1);
		textview3.setTypeface(font1);
		textview5.setTypeface(font1);
		textview7.setTypeface(font1);
		textview9.setTypeface(font1);
		textview1.setTypeface(font1);
		textview2.setTypeface(font1);
		textview4.setTypeface(font1);
		textview6.setTypeface(font1);
		textview8.setTypeface(font1);

		textview1.setText("Get After It !");

		/*
		 * SimpleDateFormat take_date = new SimpleDateFormat("MM/dd/yyyy");
		 * String system_date = take_date.format(new Date());
		 * textview9.setText(system_date);
		 */
		backButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				onBackPressed();

			}
		});

		// week 1
		if (SomolvFullActivity.click_position_full.equals("1")) {
			textview3.setText(SomolvFullActivity.wt1);
			textview0.setText("Day 1");
			textview5.setText("3");
			textview7.setText("8");

		} else if (SomolvFullActivity.click_position_full.equals("2")) {
			textview3.setText(SomolvFullActivity.wt2);
			textview0.setText("Day 1");
			textview5.setText("1");
			textview7.setText("5");

		} else if (SomolvFullActivity.click_position_full.equals("3")) {
			textview3.setText(SomolvFullActivity.wt3);
			textview0.setText("Day 1");
			textview5.setText("2");
			textview7.setText("2");
		} else if (SomolvFullActivity.click_position_full.equals("4")) {
			textview3.setText(SomolvFullActivity.wt4);
			textview0.setText("Day 1");
			textview5.setText("1");
			textview7.setText("1");

		} else if (SomolvFullActivity.click_position_full.equals("5")) {
			textview3.setText(SomolvFullActivity.wt1);
			textview0.setText("Day 2");
			textview5.setText("3");
			textview7.setText("8");
		} else if (SomolvFullActivity.click_position_full.equals("6")) {
			textview3.setText(SomolvFullActivity.wt2);
			textview0.setText("Day 2");
			textview5.setText("1");
			textview7.setText("5");
		} else if (SomolvFullActivity.click_position_full.equals("7")) {
			textview3.setText(SomolvFullActivity.wt3);
			textview0.setText("Day 2");
			textview5.setText("2");
			textview7.setText("2");
		} else if (SomolvFullActivity.click_position_full.equals("8")) {
			textview3.setText(SomolvFullActivity.wt4);
			textview0.setText("Day 2");
			textview5.setText("1");
			textview7.setText("1");
		} else if (SomolvFullActivity.click_position_full.equals("9")) {
			textview3.setText(SomolvFullActivity.wt5);
			textview0.setText("Day 3");
			textview5.setText("4");
			textview7.setText("5");
		} else if (SomolvFullActivity.click_position_full.equals("10")) {
			textview3.setText(SomolvFullActivity.wt4);
			textview0.setText("Day 3");
			textview5.setText("1");
			textview7.setText("3");
		} else if (SomolvFullActivity.click_position_full.equals("11")) {
			textview3.setText(SomolvFullActivity.wt6);
			textview0.setText("Day 3");
			textview5.setText("2");
			textview7.setText("2");
		} else if (SomolvFullActivity.click_position_full.equals("12")) {
			textview3.setText(SomolvFullActivity.wt5);
			textview0.setText("Day 3");
			textview5.setText("1");
			textview7.setText("1");
		}
		// week 2
		else if (SomolvFullActivity.click_position_full.equals("14")) {
			textview3.setText(SomolvFullActivity.wt2);
			textview0.setText("Day 1");
			textview5.setText("1");
			textview7.setText("5");
		} else if (SomolvFullActivity.click_position_full.equals("15")) {
			textview3.setText(SomolvFullActivity.wt3);
			textview0.setText("Day 2");
			textview5.setText("1");
			textview7.setText("5");
		} else if (SomolvFullActivity.click_position_full.equals("16")) {
			textview3.setText(SomolvFullActivity.wt4);
			textview0.setText("Day 3");
			textview5.setText("1");
			textview7.setText("5");
		} else if (SomolvFullActivity.click_position_full.equals("18")) {
			textview3.setText(SomolvFullActivity.wt7);
			textview0.setText("Day 1");
			textview5.setText("4");
			textview7.setText("9");
		} else if (SomolvFullActivity.click_position_full.equals("19")) {
			textview3.setText(SomolvFullActivity.wt8);
			textview0.setText("Day 2");
			textview5.setText("5");
			textview7.setText("7");
		} else if (SomolvFullActivity.click_position_full.equals("20")) {
			textview3.setText(SomolvFullActivity.wt9);
			textview0.setText("Day 3");
			textview5.setText("7");
			textview7.setText("5");
		} else if (SomolvFullActivity.click_position_full.equals("21")) {
			textview3.setText(SomolvFullActivity.wt10);
			textview0.setText("Day 4");
			textview5.setText("10");
			textview7.setText("3");
		} else if (SomolvFullActivity.click_position_full.equals("23")) {
			textview3.setText(SomolvFullActivity.wt11);
			textview0.setText("Day 1");
			textview5.setText("4");
			textview7.setText("9");
		} else if (SomolvFullActivity.click_position_full.equals("24")) {
			textview3.setText(SomolvFullActivity.wt12);
			textview0.setText("Day 2");
			textview5.setText("5");
			textview7.setText("7");
		} else if (SomolvFullActivity.click_position_full.equals("25")) {
			textview3.setText(SomolvFullActivity.wt13);
			textview0.setText("Day 3");
			textview5.setText("7");
			textview7.setText("5");
		} else if (SomolvFullActivity.click_position_full.equals("26")) {
			textview3.setText(SomolvFullActivity.wt14);
			textview0.setText("Day 4");
			textview5.setText("10");
			textview7.setText("3");
		} else if (SomolvFullActivity.click_position_full.equals("28")) {
			textview3.setText(SomolvFullActivity.wt15);
			textview0.setText("Day 1");
			textview5.setText("4");
			textview7.setText("9");
		} else if (SomolvFullActivity.click_position_full.equals("29")) {
			textview3.setText(SomolvFullActivity.wt1);
			textview0.setText("Day 2");
			textview5.setText("5");
			textview7.setText("7");
		} else if (SomolvFullActivity.click_position_full.equals("30")) {
			textview3.setText(SomolvFullActivity.wt3);
			textview0.setText("Day 3");
			textview5.setText("7");
			textview7.setText("5");
		} else if (SomolvFullActivity.click_position_full.equals("31")) {
			textview3.setText(SomolvFullActivity.wt7);
			textview0.setText("Day 4");
			textview5.setText("10");
			textview7.setText("3");
		}
		// week 6
		else if (SomolvFullActivity.click_position_full.equals("33")) {
			textview3.setText("Rest");
			textview5.setText("0");
			textview7.setText("0");
			textview0.setText("Day 1");
		} else if (SomolvFullActivity.click_position_full.equals("34")) {
			textview3.setText("Rest");
			textview5.setText("0");
			textview7.setText("0");
			textview0.setText("Day 2");
		} else if (SomolvFullActivity.click_position_full.equals("35")) {
			textview3.setText("Max");
			textview5.setText("1");
			textview7.setText("1");
			textview0.setText("Day 3");
		} else if (SomolvFullActivity.click_position_full.equals("36")) {
			textview3.setText("Max");
			textview5.setText("1");
			textview7.setText("1");
			textview0.setText("Day 4");
		}
		// week 9 day 1
		else if (SomolvFullActivity.click_position_full.equals("42")) {
			textview3.setText(SomolvFullActivity.wt5);
			textview0.setText("Day 1");
			textview5.setText("1");
			textview7.setText("3");
		} else if (SomolvFullActivity.click_position_full.equals("43")) {
			textview3.setText(SomolvFullActivity.wt7);
			textview0.setText("Day 1");
			textview5.setText("1");
			textview7.setText("4");
		} else if (SomolvFullActivity.click_position_full.equals("44")) {
			textview3.setText(SomolvFullActivity.wt1);
			textview0.setText("Day 1");
			textview5.setText("3");
			textview7.setText("4");
		} else if (SomolvFullActivity.click_position_full.equals("45")) {
			textview3.setText(SomolvFullActivity.wt2);
			textview5.setText("1");
			textview0.setText("Day 1");
			textview7.setText("5");
		}
		// week 9 day 2
		else if (SomolvFullActivity.click_position_full.equals("46")) {
			textview3.setText(SomolvFullActivity.wt4);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 2");
		} else if (SomolvFullActivity.click_position_full.equals("47")) {
			textview3.setText(SomolvFullActivity.wt0);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 2");
		} else if (SomolvFullActivity.click_position_full.equals("48")) {
			textview3.setText(SomolvFullActivity.wt2);
			textview5.setText("1");
			textview7.setText("4");
			textview0.setText("Day 2");
		} else if (SomolvFullActivity.click_position_full.equals("49")) {
			textview3.setText(SomolvFullActivity.wt4);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 2");
		} else if (SomolvFullActivity.click_position_full.equals("50")) {
			textview3.setText(SomolvFullActivity.wt5);
			textview5.setText("2");
			textview7.setText("5");
			textview0.setText("Day 2");
		} else if (SomolvFullActivity.click_position_full.equals("51")) {
			textview3.setText(SomolvFullActivity.wt7);
			textview5.setText("1");
			textview7.setText("4");
			textview0.setText("Day 3");
		} else if (SomolvFullActivity.click_position_full.equals("52")) {
			textview3.setText(SomolvFullActivity.wt1);
			textview5.setText("1");
			textview7.setText("4");
			textview0.setText("Day 3");
		} else if (SomolvFullActivity.click_position_full.equals("53")) {
			textview3.setText(SomolvFullActivity.wt2);
			textview5.setText("5");
			textview7.setText("4");
			textview0.setText("Day 3");
		}

		// week 10
		else if (SomolvFullActivity.click_position_full.equals("55")) {
			textview3.setText(SomolvFullActivity.wt4);
			textview5.setText("1");
			textview7.setText("4");
			textview0.setText("Day 1");
		} else if (SomolvFullActivity.click_position_full.equals("56")) {
			textview3.setText(SomolvFullActivity.wt0);
			textview5.setText("1");
			textview7.setText("4");
			textview0.setText("Day 1");
		} else if (SomolvFullActivity.click_position_full.equals("57")) {
			textview3.setText(SomolvFullActivity.wt2);
			textview5.setText("1");
			textview7.setText("4");
			textview0.setText("Day 1");
		} else if (SomolvFullActivity.click_position_full.equals("58")) {
			textview3.setText(SomolvFullActivity.wt4);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 1");
		} else if (SomolvFullActivity.click_position_full.equals("59")) {
			textview3.setText(SomolvFullActivity.wt5);
			textview5.setText("2");
			textview7.setText("4");
			textview0.setText("Day 1");
		} else if (SomolvFullActivity.click_position_full.equals("60")) {
			textview3.setText(SomolvFullActivity.wt5);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 2");
		} else if (SomolvFullActivity.click_position_full.equals("61")) {
			textview3.setText(SomolvFullActivity.wt1);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 2");
		} else if (SomolvFullActivity.click_position_full.equals("62")) {
			textview3.setText(SomolvFullActivity.wt3);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 2");
		} else if (SomolvFullActivity.click_position_full.equals("63")) {
			textview3.setText(SomolvFullActivity.wt7);
			textview5.setText("3");
			textview7.setText("3");
			textview0.setText("Day 2");
		} else if (SomolvFullActivity.click_position_full.equals("64")) {
			textview3.setText(SomolvFullActivity.wt5);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 2");
		} else if (SomolvFullActivity.click_position_full.equals("65")) {
			textview3.setText(SomolvFullActivity.wt16);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 3");
		} else if (SomolvFullActivity.click_position_full.equals("66")) {
			textview3.setText(SomolvFullActivity.wt1);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 3");
		} else if (SomolvFullActivity.click_position_full.equals("67")) {
			textview3.setText(SomolvFullActivity.wt3);
			textview5.setText("1");
			textview7.setText("4");
			textview0.setText("Day 3");
		} else if (SomolvFullActivity.click_position_full.equals("68")) {
			textview3.setText(SomolvFullActivity.wt7);
			textview5.setText("4");
			textview7.setText("5");
			textview0.setText("Day 3");
		}
		// week 11
		else if (SomolvFullActivity.click_position_full.equals("70")) {
			textview3.setText(SomolvFullActivity.wt5);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 1");
		} else if (SomolvFullActivity.click_position_full.equals("71")) {
			textview3.setText(SomolvFullActivity.wt0);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 1");
		} else if (SomolvFullActivity.click_position_full.equals("72")) {
			textview3.setText(SomolvFullActivity.wt2);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 1");
		} else if (SomolvFullActivity.click_position_full.equals("73")) {
			textview3.setText(SomolvFullActivity.wt4);
			textview5.setText("5");
			textview7.setText("5");
			textview0.setText("Day 1");
		} else if (SomolvFullActivity.click_position_full.equals("74")) {
			textview3.setText(SomolvFullActivity.wt5);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 2");
		} else if (SomolvFullActivity.click_position_full.equals("75")) {
			textview3.setText(SomolvFullActivity.wt2);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 2");
		} else if (SomolvFullActivity.click_position_full.equals("76")) {
			textview3.setText(SomolvFullActivity.wt4);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 2");
		} else if (SomolvFullActivity.click_position_full.equals("77")) {
			textview3.setText(SomolvFullActivity.wt16);
			textview5.setText("2");
			textview7.setText("3");
			textview0.setText("Day 2");
		} else if (SomolvFullActivity.click_position_full.equals("78")) {
			textview3.setText(SomolvFullActivity.wt1);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 3");
		} else if (SomolvFullActivity.click_position_full.equals("79")) {
			textview3.setText(SomolvFullActivity.wt3);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 3");
		} else if (SomolvFullActivity.click_position_full.equals("80")) {
			textview3.setText(SomolvFullActivity.wt7);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 3");
		} else if (SomolvFullActivity.click_position_full.equals("81")) {
			textview3.setText(SomolvFullActivity.wt16);
			textview5.setText("4");
			textview7.setText("3");
			textview0.setText("Day 3");
		}
		// week 12
		else if (SomolvFullActivity.click_position_full.equals("83")) {
			textview3.setText(SomolvFullActivity.wt2);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 1");
		} else if (SomolvFullActivity.click_position_full.equals("84")) {
			textview3.setText(SomolvFullActivity.wt4);
			textview5.setText("1");
			textview7.setText("4");
			textview0.setText("Day 1");
		} else if (SomolvFullActivity.click_position_full.equals("85")) {
			textview3.setText(SomolvFullActivity.wt5);
			textview5.setText("5");
			textview7.setText("5");
			textview0.setText("Day 1");
		} else if (SomolvFullActivity.click_position_full.equals("86")) {
			textview3.setText(SomolvFullActivity.wt2);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 2");
		} else if (SomolvFullActivity.click_position_full.equals("87")) {
			textview3.setText(SomolvFullActivity.wt4);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 2");
		} else if (SomolvFullActivity.click_position_full.equals("88")) {
			textview3.setText(SomolvFullActivity.wt16);
			textview5.setText("4");
			textview7.setText("3");
			textview0.setText("Day 2");
		} else if (SomolvFullActivity.click_position_full.equals("89")) {
			textview3.setText(SomolvFullActivity.wt3);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 3");
		} else if (SomolvFullActivity.click_position_full.equals("90")) {
			textview3.setText(SomolvFullActivity.wt4);
			textview5.setText("1");
			textview7.setText("4");
			textview0.setText("Day 3");
		} else if (SomolvFullActivity.click_position_full.equals("91")) {
			textview3.setText(SomolvFullActivity.wt5);
			textview5.setText("3");
			textview7.setText("4");
			textview0.setText("Day 3");
		}
		// Tapper Week
		else if (SomolvFullActivity.click_position_full.equals("93")) {
			textview3.setText(SomolvFullActivity.wt2);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 1");
		} else if (SomolvFullActivity.click_position_full.equals("94")) {
			textview3.setText(SomolvFullActivity.wt4);
			textview5.setText("1");
			textview7.setText("3");
			textview0.setText("Day 1");
		} else if (SomolvFullActivity.click_position_full.equals("95")) {
			textview3.setText(SomolvFullActivity.wt5);
			textview5.setText("2");
			textview7.setText("5");
			textview0.setText("Day 1");
		} else if (SomolvFullActivity.click_position_full.equals("96")) {
			textview3.setText(SomolvFullActivity.wt16);
			textview5.setText("3");
			textview7.setText("4");
			textview0.setText("Day 1");
		} else if (SomolvFullActivity.click_position_full.equals("97")) {
			textview3.setText(SomolvFullActivity.wt2);
			textview5.setText("1");
			textview7.setText("4");
			textview0.setText("Day 2");
		} else if (SomolvFullActivity.click_position_full.equals("98")) {
			textview3.setText(SomolvFullActivity.wt4);
			textview5.setText("4");
			textview7.setText("4");
			textview0.setText("Day 2");
		} else if (SomolvFullActivity.click_position_full.equals("99")) {
			textview3.setText(SomolvFullActivity.wt4);
			textview5.setText("1");
			textview7.setText("1");
			textview0.setText("Day 3");
		}

	}
	
	@Override
	public void onBackPressed() {
//		Intent myIntent = new Intent(GetAfterItActivity_Full.this,
//				SomolvFullActivity.class);
//		startActivity(myIntent);
		finish();
	}

}
