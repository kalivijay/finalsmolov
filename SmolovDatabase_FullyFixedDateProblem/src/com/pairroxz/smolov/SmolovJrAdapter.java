package com.pairroxz.smolov;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class SmolovJrAdapter extends ArrayAdapter<Item> implements
		OnCheckedChangeListener {

	private List<Item> items;
	private LayoutInflater inflater;
	private SmolovJr smolovJr;
	private SectionHeader header;
	private SmolovHolder smolovHolder;
	private SectionHeaderHolder sectionHolder;
	private SharedPreferences prefs;
	private SharedPreferences.Editor prefs_edit;
	private String check_list;
	private Boolean[] arrChecks;

	private Typeface font1;

	SimpleDateFormat take_date = new SimpleDateFormat("MM/dd/yyyy");
	String system_date = take_date.format(new Date());

	// Variables for Data Settings
	Boolean[] arrChecks_vijay;
	static Boolean[] store_date;

	public SmolovJrAdapter(Context context, List<Item> items) {
		super(context, 0, items);
		this.items = items;

		font1 = Typeface.createFromAsset(context.getAssets(),
				"fonts/Seravek.otf");
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		prefs = PreferenceManager.getDefaultSharedPreferences(context
				.getApplicationContext());
		prefs_edit = prefs.edit();

		check_list = prefs.getString("check_list_1", null);
		arrChecks = new Boolean[items.size()];
		for (int i = 0; i < items.size(); i++) {
			arrChecks[i] = false;
		}

		if (check_list != null) {
			check_list = check_list.replace("[", "").replace("]", "")
					.replace(" ", "");
			String strTmpArr[] = check_list.split(",");
			for (int i = 0; i < items.size(); i++) {
				arrChecks[i] = Boolean.parseBoolean(strTmpArr[i]);
			}
		}

		store_date = arrChecks;
		Log.d("Final Time", "------------------------");

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		final Item item = items.get(position);
		if (item.isSection()) {
			header = (SectionHeader) item;
			row = inflater.inflate(R.layout.listview_secton, null);
			sectionHolder = new SectionHeaderHolder(row);
			sectionHolder.txHeader.setText(header.title);
		} else {
			smolovJr = (SmolovJr) item;
			row = inflater.inflate(R.layout.listview_item_row, null);
			smolovHolder = new SmolovHolder(row);
			smolovHolder.txtTitle1.setText(smolovJr.day);
			smolovHolder.txtTitle2.setText(smolovJr.sets);
			smolovHolder.txtTitle3.setText(smolovJr.reps);
			smolovHolder.txtTitle4.setText(smolovJr.weight);
			smolovHolder.checkbox1.setTag(position);
			smolovHolder.checkbox1.setChecked(arrChecks[position]);
			smolovHolder.checkbox1.setOnCheckedChangeListener(this);
		}
		notifyDataSetChanged();
		return row;
	}

	class SmolovHolder {
		TextView txtTitle1, txtTitle2, txtTitle3, txtTitle4, txtTitle5;
		CheckBox checkbox1;

		public SmolovHolder(View view) {
			txtTitle1 = (TextView) view.findViewById(R.id.txtTitle1);
			txtTitle2 = (TextView) view.findViewById(R.id.txtTitle2);
			txtTitle3 = (TextView) view.findViewById(R.id.txtTitle3);
			txtTitle4 = (TextView) view.findViewById(R.id.txtTitle4);
			checkbox1 = (CheckBox) view.findViewById(R.id.checkbox1);

			txtTitle1.setTypeface(font1);
			txtTitle2.setTypeface(font1);
			txtTitle3.setTypeface(font1);
			txtTitle4.setTypeface(font1);
		}
	}

	class SectionHeaderHolder {
		TextView txHeader;

		public SectionHeaderHolder(View view) {
			txHeader = (TextView) view.findViewById(R.id.txtHeader);
			txHeader.setTypeface(font1);
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton checkbox, boolean checked) {

		arrChecks[Integer.parseInt(checkbox.getTag().toString())] = checked;
		String tempArrStr = Arrays.toString(arrChecks);

		// Log.d("Good Time", )

		store_date = arrChecks;

		if (!store_date[1]) {

			prefs_edit.putString("date1", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("date1", "").equalsIgnoreCase("")) {
				prefs_edit.putString("date1", system_date);
				prefs_edit.commit();
			}
		}

		// ----------------
		if (!store_date[2]) {

			prefs_edit.putString("date2", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("date2", "").equalsIgnoreCase("")) {
				prefs_edit.putString("date2", system_date);
				prefs_edit.commit();
			}

		}
		// --------------
		if (!store_date[3]) {

			prefs_edit.putString("date3", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("date3", "").equalsIgnoreCase("")) {
				prefs_edit.putString("date3", system_date);
				prefs_edit.commit();
			}

		}
		// ----------------------
		if (!store_date[4]) {

			prefs_edit.putString("date4", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("date4", "").equalsIgnoreCase("")) {
				prefs_edit.putString("date4", system_date);
				prefs_edit.commit();
			}

		}
		// ------------------
		if (!store_date[6]) {

			prefs_edit.putString("date6", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("date6", "").equalsIgnoreCase("")) {
				prefs_edit.putString("date6", system_date);
				prefs_edit.commit();
			}

		}
		// --------------------------

		if (!store_date[7]) {

			prefs_edit.putString("date7", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("date7", "").equalsIgnoreCase("")) {
				prefs_edit.putString("date7", system_date);
				prefs_edit.commit();
			}
		}
		// --------------------------------------

		if (!store_date[8]) {

			prefs_edit.putString("date8", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("date8", "").equalsIgnoreCase("")) {
				prefs_edit.putString("date8", system_date);
				prefs_edit.commit();
			}
		}
		// ----------------------

		if (!store_date[9]) {

			prefs_edit.putString("date9", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("date9", "").equalsIgnoreCase("")) {
				prefs_edit.putString("date9", system_date);
				prefs_edit.commit();
			}
		}
		// -----------------------
		if (!store_date[11]) {

			prefs_edit.putString("date11", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("date11", "").equalsIgnoreCase("")) {
				prefs_edit.putString("date11", system_date);
				prefs_edit.commit();
			}
		}
		// ---------------------
		if (!store_date[12]) {

			prefs_edit.putString("date12", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("date12", "").equalsIgnoreCase("")) {
				prefs_edit.putString("date12", system_date);
				prefs_edit.commit();
			}
		}
		// -------------------
		if (!store_date[13]) {

			prefs_edit.putString("date13", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("date13", "").equalsIgnoreCase("")) {
				prefs_edit.putString("date13", system_date);
				prefs_edit.commit();
			}
		}
		// -------------------
		if (!store_date[14]) {

			prefs_edit.putString("date14", "");
			prefs_edit.commit();
		} else {
			if (prefs.getString("date14", "").equalsIgnoreCase("")) {
				prefs_edit.putString("date14", system_date);
				prefs_edit.commit();
			}
		}
		// ----------------------

		for (int i = 0; i < items.size(); i++) {

			Log.d("Final Time", store_date[i] + "");

		}

		Log.d("Final Time", "---------------------------");

		prefs_edit.putString("check_list_1", tempArrStr);
		prefs_edit.commit();

		// //check_list = prefs.getString("check_list_1", null);
		// arrChecks_vijay = new Boolean[items.size()];
		// for (int i = 0; i < items.size(); i++) {
		// arrChecks_vijay[i] = false;
		// }
		//
		// if (check_list != null) {
		// check_list = check_list.replace("[", "").replace("]", "")
		// .replace(" ", "");
		// String strTmpArr[] = check_list.split(",");
		// for (int i = 0; i < items.size(); i++) {
		// arrChecks_vijay[i] = Boolean.parseBoolean(strTmpArr[i]);
		// // Log.d("Time", arrChecks[i] + " " + i);
		// }
		// }

		// if (arrChecks[1] == arrChecks_vijay[1]) {
		// Toast.makeText(getContext(),
		// "same"+arrChecks[1]+"  "+arrChecks_vijay[1], Toast.LENGTH_SHORT)
		// .show();
		// }
		// else{
		// Toast.makeText(getContext(), "different", Toast.LENGTH_SHORT)
		// .show();
		// }

		// for (int i = 0; i < items.size(); i++) {
		//
		// Log.d("Time", store_date[i] + " " + i);
		// }

		/*
		 * SimpleDateFormat take_date = new SimpleDateFormat("MM/dd/yyyy"); if
		 * (arrChecks[1]) {
		 * 
		 * String system_date = take_date.format(new Date());
		 * prefs_edit.putString("date2", system_date); prefs_edit.commit();
		 * 
		 * } else if (arrChecks[2]) {
		 * 
		 * String system_date = take_date.format(new Date());
		 * prefs_edit.putString("date2", system_date); prefs_edit.commit();
		 * 
		 * } else if (arrChecks[3]) {
		 * 
		 * String system_date = take_date.format(new Date());
		 * prefs_edit.putString("date3", system_date); prefs_edit.commit();
		 * 
		 * } else if (arrChecks[4]) {
		 * 
		 * String system_date = take_date.format(new Date());
		 * prefs_edit.putString("date4", system_date); prefs_edit.commit();
		 * 
		 * } else if (arrChecks[6]) {
		 * 
		 * String system_date = take_date.format(new Date());
		 * prefs_edit.putString("date6", system_date); prefs_edit.commit();
		 * 
		 * } else if (arrChecks[7]) {
		 * 
		 * String system_date = take_date.format(new Date());
		 * prefs_edit.putString("date7", system_date); prefs_edit.commit();
		 * 
		 * } else if (arrChecks[8]) {
		 * 
		 * String system_date = take_date.format(new Date());
		 * prefs_edit.putString("date8", system_date); prefs_edit.commit();
		 * 
		 * } else if (arrChecks[9]) {
		 * 
		 * String system_date = take_date.format(new Date());
		 * prefs_edit.putString("date9", system_date); prefs_edit.commit();
		 * 
		 * } else if (arrChecks[11]) {
		 * 
		 * String system_date = take_date.format(new Date());
		 * prefs_edit.putString("date11", system_date); prefs_edit.commit();
		 * 
		 * } else if (arrChecks[12]) {
		 * 
		 * String system_date = take_date.format(new Date());
		 * prefs_edit.putString("date12", system_date); prefs_edit.commit();
		 * 
		 * } else if (arrChecks[13]) {
		 * 
		 * String system_date = take_date.format(new Date());
		 * prefs_edit.putString("date13", system_date); prefs_edit.commit();
		 * 
		 * } else if (arrChecks[14]) {
		 * 
		 * String system_date = take_date.format(new Date());
		 * prefs_edit.putString("date14", system_date); prefs_edit.commit();
		 * 
		 * }
		 */
	}
}
