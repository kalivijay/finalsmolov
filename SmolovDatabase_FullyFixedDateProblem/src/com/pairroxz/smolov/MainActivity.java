package com.pairroxz.smolov;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends Activity {

	TextView top_title;
	ImageView imageButton1, imageButton2, imageButton3;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getWindow().requestFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.activity_main);
		imageButton1 = (ImageView) findViewById(R.id.imageButton1);
		imageButton2 = (ImageView) findViewById(R.id.imageButton2);
		imageButton3 = (ImageView) findViewById(R.id.imageButton3);
		top_title = (TextView) findViewById(R.id.top_title);

		Typeface font1 = Typeface.createFromAsset(getAssets(),
				"fonts/Seravek.otf");

		top_title.setTypeface(font1);
		
		top_title.setText("SmovlovJr.com Calculators");

		imageButton1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent Myintent = new Intent(MainActivity.this,
						SomolvJrCalActivity.class);
				startActivity(Myintent);
				finish();
			}
		});

		imageButton2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				Intent Myintent = new Intent(MainActivity.this,
						SomolvFullActivity.class);

				startActivity(Myintent);
				finish();
			}
		});

		imageButton3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent Myintent = new Intent(MainActivity.this, Settings.class);
				startActivity(Myintent);
				finish();
			}

		});

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		finish();
	}
}
