package com.pairroxz.smolov;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class DayComplete extends Activity {

	ImageView imageview1;
	TextView textview3, textview5, textview7, textview9, textview1, textview10,
			textview2, textview4, textview6, textview8;
	TextView textview0;
	ImageButton backButton;
	private SharedPreferences prefs;
	private SharedPreferences.Editor prefs_edit;

	@SuppressLint("SimpleDateFormat")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.activity_day_complete);
		Typeface font1 = Typeface.createFromAsset(getAssets(),
				"fonts/Seravek.otf");

		prefs = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		prefs_edit = prefs.edit();

		imageview1 = (ImageView) findViewById(R.id.imageview1);
		textview1 = (TextView) findViewById(R.id.textview1);
		textview3 = (TextView) findViewById(R.id.textview3);
		textview5 = (TextView) findViewById(R.id.textview5);
		textview7 = (TextView) findViewById(R.id.textview7);
		textview9 = (TextView) findViewById(R.id.textview9);
		textview0 = (TextView) findViewById(R.id.textview0);
		textview10 = (TextView) findViewById(R.id.textview10);
		textview2 = (TextView) findViewById(R.id.textview2);
		textview4 = (TextView) findViewById(R.id.textview4);
		textview6 = (TextView) findViewById(R.id.textview6);
		textview8 = (TextView) findViewById(R.id.textview8);
		backButton = (ImageButton) findViewById(R.id.backButton);
		textview0.setTypeface(font1);
		textview3.setTypeface(font1);
		textview5.setTypeface(font1);
		textview7.setTypeface(font1);
		textview9.setTypeface(font1);
		textview1.setTypeface(font1);
		textview10.setTypeface(font1);
		textview2.setTypeface(font1);
		textview4.setTypeface(font1);
		textview6.setTypeface(font1);
		textview8.setTypeface(font1);

		textview1.setText("You Crushed It !!");
		textview10.setText("Awesome Job !!");

		SimpleDateFormat take_date = new SimpleDateFormat("MM/dd/yyyy");
		String system_date = take_date.format(new Date());

		backButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				onBackPressed();

			}
		});

		if (SomolvJrCalActivity.click_position.equals("1")) {
			textview3.setText(SomolvJrCalActivity.Wt1);
			textview0.setText("Day 1");
			textview5.setText("6");
			textview7.setText("6");
			Log.d("FormHome", "yyyyyyyyyyyyyyyyy");
			if (SmolovJrAdapter.store_date[1]) {
				// when checkbox click after first time or false to ture. means
				// there is no date.
				if (prefs.getString("date1", "").equalsIgnoreCase("")) {
					prefs_edit.putString("date1", system_date);
					prefs_edit.commit();
					textview9.setText(prefs.getString("date1", ""));
				}
				// if there is already a date.
				else {

					textview9.setText(prefs.getString("date1", ""));
				}

			}
			// when checkbox is false make it empty --->NEVER EXECUTE
			else {

				prefs_edit.putString("date1", "");
				prefs_edit.commit();
			}
			Log.d("FormHome", "nnnnnnnnnnnnnnnnnnnnnnn");

		} else if (SomolvJrCalActivity.click_position.equals("2")) {
			textview3.setText(SomolvJrCalActivity.Wt2);
			textview0.setText("Day 2");
			textview5.setText("7");
			textview7.setText("5");
			if (prefs.getString("date2", "").equalsIgnoreCase("")) {
				prefs_edit.putString("date2", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("date2", ""));
			} else {

				textview9.setText(prefs.getString("date2", ""));
			}

		} else if (SomolvJrCalActivity.click_position.equals("3")) {
			textview3.setText(SomolvJrCalActivity.Wt3);
			textview0.setText("Day 3");
			textview5.setText("8");
			textview7.setText("4");
			if (prefs.getString("date3", "").equalsIgnoreCase("")) {
				prefs_edit.putString("date3", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("date3", ""));
			} else {

				textview9.setText(prefs.getString("date3", ""));
			}
		} else if (SomolvJrCalActivity.click_position.equals("4")) {
			textview3.setText(SomolvJrCalActivity.Wt4);
			textview0.setText("Day 4");
			textview5.setText("10");
			textview7.setText("3");
			if (prefs.getString("date4", "").equalsIgnoreCase("")) {
				prefs_edit.putString("date4", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("date4", ""));
			} else {

				textview9.setText(prefs.getString("date4", ""));
			}
		} else if (SomolvJrCalActivity.click_position.equals("6")) {
			textview3.setText(SomolvJrCalActivity.Wt5);
			textview0.setText("Day 1");
			textview5.setText("6");
			textview7.setText("6");
			if (prefs.getString("date6", "").equalsIgnoreCase("")) {
				prefs_edit.putString("date6", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("date6", ""));
			} else {

				textview9.setText(prefs.getString("date6", ""));
			}
		} else if (SomolvJrCalActivity.click_position.equals("7")) {
			textview3.setText(SomolvJrCalActivity.Wt6);
			textview0.setText("Day 2");
			textview5.setText("7");
			textview7.setText("5");
			if (prefs.getString("date7", "").equalsIgnoreCase("")) {
				prefs_edit.putString("date7", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("date7", ""));
			} else {

				textview9.setText(prefs.getString("date7", ""));
			}
		} else if (SomolvJrCalActivity.click_position.equals("8")) {
			textview3.setText(SomolvJrCalActivity.Wt7);
			textview0.setText("Day 3");
			textview5.setText("8");
			textview7.setText("4");
			if (prefs.getString("date8", "").equalsIgnoreCase("")) {
				prefs_edit.putString("date8", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("date8", ""));
			} else {

				textview9.setText(prefs.getString("date8", ""));
			}

		} else if (SomolvJrCalActivity.click_position.equals("9")) {
			textview3.setText(SomolvJrCalActivity.Wt8);
			textview0.setText("Day 4");
			textview5.setText("10");
			textview7.setText("3");
			if (prefs.getString("date9", "").equalsIgnoreCase("")) {
				prefs_edit.putString("date9", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("date9", ""));
			} else {

				textview9.setText(prefs.getString("date9", ""));
			}
		} else if (SomolvJrCalActivity.click_position.equals("11")) {
			textview3.setText(SomolvJrCalActivity.Wt9);
			textview0.setText("Day 1");
			textview5.setText("6");
			textview7.setText("6");
			if (prefs.getString("date11", "").equalsIgnoreCase("")) {
				prefs_edit.putString("date11", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("date11", ""));
			} else {

				textview9.setText(prefs.getString("date11", ""));
			}
		} else if (SomolvJrCalActivity.click_position.equals("12")) {
			textview3.setText(SomolvJrCalActivity.Wt10);
			textview0.setText("Day 2");
			textview5.setText("7");
			textview7.setText("5");
			if (prefs.getString("date12", "").equalsIgnoreCase("")) {
				prefs_edit.putString("date12", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("date12", ""));
			} else {

				textview9.setText(prefs.getString("date12", ""));
			}
		} else if (SomolvJrCalActivity.click_position.equals("13")) {
			textview3.setText(SomolvJrCalActivity.Wt11);
			textview0.setText("Day 3");
			textview5.setText("8");
			textview7.setText("4");
			if (prefs.getString("date13", "").equalsIgnoreCase("")) {
				prefs_edit.putString("date13", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("date13", ""));
			} else {

				textview9.setText(prefs.getString("date13", ""));
			}
		} else if (SomolvJrCalActivity.click_position.equals("14")) {
			textview3.setText(SomolvJrCalActivity.Wt12);
			textview0.setText("Day 4");
			textview5.setText("10");
			textview7.setText("3");
			if (prefs.getString("date14", "").equalsIgnoreCase("")) {
				prefs_edit.putString("date14", system_date);
				prefs_edit.commit();
				textview9.setText(prefs.getString("date14", ""));
			} else {

				textview9.setText(prefs.getString("date14", ""));
			}
		}

	}

	@Override
	public void onBackPressed() {
		Intent myIntent = new Intent(DayComplete.this,
				SomolvJrCalActivity.class);
		startActivity(myIntent);
		finish();
	}
}
